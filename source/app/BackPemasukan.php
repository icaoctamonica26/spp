<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BackPemasukan extends Model
{
     /**
     * Nama table yang digunakan
     */
    protected $table = 'back_pemasukan';

    /**
     * Kolom yang dapat di isi
     */

     protected $fillable = [
     	"user",
        "title",
        "description",
        "sumber",
        "foto",
        "tipe",
        "nominal",
        "keterangan",
        "created_at",
        "updated_at",
    ];
}
