<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BackPembayaran extends Model
{
    protected $table = 'back_pembayaran';
    protected $fillable = [
        "id_student",
        "kategori_pembayaran",
        "tanggal_pembayaran",
        "nominal",
        "keterangan",
        "penerima",
        // "created_at",
        // "updated_at",
    ];
}
