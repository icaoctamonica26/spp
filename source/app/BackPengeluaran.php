<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BackPengeluaran extends Model
{
    /**
     * Nama table yang digunakan
     */
    protected $table = 'back_pengeluaran';

    /**
     * Kolom yang dapat di isi
     */
    protected $fillable = [
        "title",
        "description",
        "sumber",
        "foto",
        "nominal",
        "tipe",
        "user",
        "keterangan",
        "created_at",
        "updated_at",

    ];
}
