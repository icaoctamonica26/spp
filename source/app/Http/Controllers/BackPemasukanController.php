<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use App\Income;
use App\Pencatatan;
use App\BackPemasukan;
use Illuminate\Support\Facades\Session;
use Auth;
use Storage;

class BackPemasukanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = BackPemasukan::orderBy('updated_at', 'desc')
                ->where('sumber','<>','Siswa')->get();
        $no=1;
        $bulan = BackPemasukan::selectRaw('MONTH(created_at) AS bulan')
                ->groupBy('bulan')
                ->orderBy('bulan')
                // ->get();
                ->where('sumber','<>','Siswa')->get();
        $tahun = BackPemasukan::selectRaw('YEAR(created_at) AS tahun')
                ->groupBy('tahun')
                ->orderBy('tahun')
                // ->get();
                ->where('sumber','<>','Siswa')->get();
        $tanggals = BackPemasukan::selectRaw('DATE_FORMAT(created_at, "%W, %d-%M-%Y") as tanggal, DATE(created_at) as tanggal_value')
                ->groupBy('tanggal_value')
                ->orderBy('created_at','DESC')
                // ->whereNull('payment_detail_id')
                // ->whereNull('cicilan_id')
                ->get();
        $report['bulan'] = "";
        $report['tahun'] = "";
        return view('controlling.backpemasukan.index', compact('datas','no','bulan','tahun', 'report','tanggals'));

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    }

    public function filter(Request $request)
    {
        if(isset($request->bulan) && isset($request->tahun)){
            $datas = BackPemasukan::orderBy('updated_at', 'desc')
                    ->whereMonth('created_at','=',$request->bulan)
                    ->whereYear('created_at','=',$request->tahun)
                    ->where('sumber','<>','Siswa')->get();
        }
        elseif(isset($request->tahun)){
            $datas = BackPemasukan::orderBy('updated_at', 'desc')
                    ->whereYear('created_at','=',$request->tahun)
                    ->where('sumber','<>','Siswa')->get();
        }
        elseif(isset($request->bulan)){
            $datas = BackPemasukan::orderBy('updated_at', 'desc')
                    ->whereMonth('created_at','=',$request->bulan)
                    ->where('sumber','<>','Siswa')->get();
        }else{
            return redirect()
                    ->route('backpemasukan.index');
        }

        $no=1;
        $bulan = BackPemasukan::selectRaw('MONTH(created_at) AS bulan')
                ->groupBy('bulan')
                ->orderBy('bulan')
                // ->get();
                ->where('sumber','<>','Siswa')->get();
        $tahun = BackPemasukan::selectRaw('YEAR(created_at) AS tahun')
                ->groupBy('tahun')
                ->orderBy('tahun')
                // ->get();
                ->where('sumber','<>','Siswa')->get();
        $tanggals = BackPemasukan::selectRaw('DATE_FORMAT(created_at, "%W, %d-%M-%Y") as tanggal, DATE(created_at) as tanggal_value')
                ->groupBy('tanggal_value')
                ->orderBy('created_at','DESC');
        $report['bulan'] = $request->bulan;
        $report['tahun'] = $request->tahun;
        return view('controlling.backpemasukan.index', compact('datas','no','bulan','tahun', 'report', 'tanggals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            BackPemasukan::findOrFail($id)->delete();

            return redirect()
                ->route('backpemasukan.index')
                ->with('success', 'Data pemasukan berhasil dihapus!');
  
          } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return redirect()
                ->route('backpemasukan.index')
                ->with('error', 'Data pemasukan gagal dihapus!');
          }
    }
}
