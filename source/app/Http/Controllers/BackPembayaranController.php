<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BackPembayaran;
use Input;
use DB;

class BackPembayaranController extends Controller
{
     public function __construct()
    {

    $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = DB::table('back_pembayaran')
                ->select('back_pembayaran.*','students.nama','students.id as id_student')
                ->join('students','students.id','back_pembayaran.id_student')
                ->orderBy('students.nama', 'ASC')
                ->groupBy('back_pembayaran.id_student')
                ->get();
        // $datas = BackPembayaran::orderBy('updated_at', 'desc')
        //         ->get();
        $no=1;
        $bulan = BackPembayaran::selectRaw('MONTH(created_at) AS bulan')
                ->groupBy('bulan')
                ->orderBy('bulan')
                // ->get();
                ->get();
        $tahun = BackPembayaran::selectRaw('YEAR(created_at) AS tahun')
                ->groupBy('tahun')
                ->orderBy('tahun')
                // ->get();
                ->get();
        $tanggals = BackPembayaran::selectRaw('DATE_FORMAT(created_at, "%W, %d-%M-%Y") as tanggal, DATE(created_at) as tanggal_value')
                ->groupBy('tanggal_value')
                ->orderBy('created_at','DESC')
                // ->whereNull('payment_detail_id')
                // ->whereNull('cicilan_id')
                ->get();
        $report['bulan'] = "";
        $report['tahun'] = "";
        return view('controlling.backpembayaran.index', compact('datas','no','bulan','tahun', 'report','tanggals'));
    }
    public function detailBackPembayaran($id)
    {
        $datas = DB::table('back_pembayaran')
                ->select('financing_categories.*','back_pembayaran.kategori_pembayaran','back_pembayaran.id_student as id')
                ->join('payments','payments.id','back_pembayaran.kategori_pembayaran')
                ->join('financing_categories','financing_categories.id','payments.financing_category_id')
                ->orderBy('financing_categories.nama', 'ASC')
                ->groupBy('financing_categories.id')
                ->where('back_pembayaran.id_student',$id)
                ->get();
        $no = 1;
        return view('controlling.backpembayaran.detailBackPembayaran', compact('datas', 'no'));
    }
    public function detailBackPembayaran2($id_student, $kategori_pembayaran)
    { 
        $datas = DB::table('back_pembayaran')
            ->where([
                ['back_pembayaran.id_student',$id_student],
                ['back_pembayaran.kategori_pembayaran',$kategori_pembayaran]
            ])
            ->get();
        $no = 1;
        return view('controlling.backpembayaran.detailBackPembayaran2', compact('datas', 'no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::table('back_pembayaran')->where('id_student',$id)->delete();
            // BackPembayaran::findOrFail($id)->delete();

            return redirect()
                ->route('backpembayaran.index')
                ->with('success', 'Data pemasukan berhasil dihapus!');
  
          } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return redirect()
                ->route('backpembayaran.index')
                ->with('error', 'Data pemasukan gagal dihapus!');
          }
    }
    public function destroy2($id)
    {
        try {
            DB::table('back_pembayaran')->where('id',$id)->delete();
            // BackPembayaran::findOrFail($id)->delete();

            return redirect()->back()
                ->with('success', 'Data pemasukan berhasil dihapus!');
  
          } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return redirect()->back()
                ->with('error', 'Data pemasukan gagal dihapus!');
          }
    }
}
