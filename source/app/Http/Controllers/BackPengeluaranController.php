<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BackPengeluaran;
use App\Expense;


class BackPengeluaranController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = BackPengeluaran::orderBy('updated_at', 'desc')->get();
        $no=1;
        $bulan = BackPengeluaran::selectRaw('MONTH(created_at) AS bulan')
                ->groupBy('bulan')
                ->orderBy('bulan')
                ->get();
        $tahun = BackPengeluaran::selectRaw('YEAR(created_at) AS tahun')
                ->groupBy('tahun')
                ->orderBy('tahun')
                ->get();
        $bln1 = '';
        $thn1 = '';
        return view('controlling.backpengeluaran.index', compact('datas','no','bulan','tahun','bln1','thn1'));
    }

    public function filter(Request $request)
    {
        if ($request->bulan == '' && $request->tahun!='') {
            $datas = BackPengeluaran::orderBy('updated_at', 'desc')
                ->whereYear('created_at',$request->tahun)
                ->get();
        }elseif ($request->bulan != '' && $request->tahun=='') {
            $datas = BackPengeluaran::orderBy('updated_at', 'desc')
                ->whereMonth('created_at',$request->bulan)
                ->get();
        }elseif ($request->bulan == '' && $request->tahun=='') {
            $datas = BackPengeluaran::orderBy('updated_at', 'desc')->get();
        }else{
            $datas = BackPengeluaran::orderBy('updated_at', 'desc')
                ->whereMonth('created_at',$request->bulan)
                ->whereYear('created_at',$request->tahun)
                ->get();
        }
        $bln1 = $request->bulan;
        $thn1 = $request->tahun;
        $no=1;
        $bulan = BackPengeluaran::selectRaw('MONTH(created_at) AS bulan')
                ->groupBy('bulan')
                ->orderBy('bulan')
                ->get();
        $tahun = BackPengeluaran::selectRaw('YEAR(created_at) AS tahun')
                ->groupBy('tahun')
                ->orderBy('tahun')
                ->get();
        return view('controlling.backpengeluaran.index', compact('datas','no','bulan','tahun','bln1','thn1'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            BackPengeluaran::findOrFail($id)->delete();

            return redirect()
                ->route('backpengeluaran.index')
                ->with('success', 'Data pemasukan berhasil dihapus!');
  
          } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return redirect()
                ->route('backpengeluaran.index')
                ->with('error', 'Data pemasukan gagal dihapus!');
          }
    }
}
