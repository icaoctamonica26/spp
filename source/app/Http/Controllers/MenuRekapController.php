<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cicilan;
use App\PaymentDetail;
use App\Payment;
use App\Pencatatan;
use App\Expense;
use App\Income;
use App\Angkatan;
use App\Student;
use App\ViewLaporanTunggakan;
use Auth;
use Illuminate\Support\Facades\DB;
use PDF;

use App\Http\Controllers\PerhitunganTunggakanSiswaController;

class MenuRekapController extends Controller
{
    public $ptsc;

    public function __construct()
    {
        $p = new PerhitunganTunggakanSiswaController;
        $this->ptsc = $p;
        set_time_limit(300);
        $this->middleware('auth');
    }
    
    public function indexPemasukan()
    {
        $no = 1;
        $total = 0;
        $response = [];
        $datas = Pencatatan::orderBy('pencatatans.updated_at', 'desc')
            ->join('incomes','incomes.id','=','pencatatans.income_id')
            ->limit(10)
            ->where([
                ['debit','<>','0'],
            ])
            ->get();
        $filter = '';
        $pilihan = '';
        return view('export.menu.pemasukan',compact('no','datas', 'filter', 'pilihan'));
    }

    public function indexPemasukanFilter(Request $request)
    {
        $no = 1;
        $filter = $request->filter;
        $pilihan = $request->pilihan;
        switch ($request->filter) {
            case 'harian':
                //Query Filter Harian
                $datas = Pencatatan::orderBy('incomes.updated_at', 'desc')
                    ->join('incomes','incomes.id','=','pencatatans.income_id')
                    ->whereDate('incomes.created_at', $request->pilihan)
                    ->get();
                break;

            case 'bulanan':
                //Query Filter Bulanan
                $temp = $request->pilihan;
                $_temp = explode('-', $temp);
                $datas = Pencatatan::orderBy('incomes.updated_at', 'desc')
                    ->join('incomes','incomes.id','=','pencatatans.income_id')
                    ->whereMonth('incomes.created_at', $_temp[1])
                    ->whereYear('incomes.created_at', $_temp[0])
                    ->get();
                break;

            case 'tahunan':
                //Query Filter Tahun
                $datas = Pencatatan::orderBy('incomes.updated_at', 'desc')
                    ->join('incomes','incomes.id','=','pencatatans.income_id')
                    ->whereYear('incomes.created_at', '=', $request->pilihan)
                    ->where([
                        ['debit','<>','0'],
                    ])
                    ->get();
                break;
            
            default:
                //Query Filter Tahun
                $datas = Pencatatan::orderBy('incomes.updated_at', 'desc')
                    ->join('incomes','incomes.id','=','pencatatans.income_id')
                    ->where([
                        ['debit','<>','0'],
                    ])
                    ->get();
        }
        return view('export.menu.pemasukan',compact('no','datas', 'filter', 'pilihan'));
    }
    
    public function indexPengeluaran()
    {
        $no = 1;
        $datas = Expense::orderBy('expenses.updated_at', 'desc')
            ->get();
        $filter = '';
        $pilihan = '';
        return view('export.menu.pengeluaran',compact('no','datas','filter','pilihan'));
    }

    public function indexPengeluaranFilter(Request $request)
    {
        $no = 1;
        switch ($request->filter) {
            case 'harian':
                //Query Filter Harian
                $datas = Expense::orderBy('expenses.updated_at', 'desc')
                    ->whereDate('expenses.created_at', $request->pilihan)
                    ->get();
                break;

            case 'bulanan':
                //Query Filter Bulanan
                $temp = $request->pilihan;
                $_temp = explode('-', $temp);
                $datas = Expense::orderBy('expenses.updated_at', 'desc')
                    ->whereMonth('expenses.created_at', $_temp[1])
                    ->whereYear('expenses.created_at', $_temp[0])
                    ->get();
                break;

            case 'tahunan':
                //Query Filter Tahun
                $datas = Expense::orderBy('expenses.updated_at', 'desc')
                    ->whereYear('expenses.created_at', '=', $request->pilihan)
                    ->get();
                break;
            default:
                $datas = Expense::orderBy('expenses.updated_at', 'desc')
                    ->get();
        }
        $filter = $request->filter;
        $pilihan = $request->pilihan;
        return view('export.menu.pengeluaran',compact('no','datas','filter','pilihan'));
    }

    public function ajaxIndexTunggakan($stat = 'Siswa')
    {
        if ($stat == 'Siswa') {
            return DB::table('payment_details')
                    ->select(DB::raw('financing_categories.id as financing_category_id,
                    payment_details.id as detail_id, 
                    payment_details.payment_id,
                    payment_details.payment_periode_id,
                    angkatans.id as angkatan_id,
                    majors.id as major_id,
                    students.nama as nama_murid,
                    students.kelas,
                    angkatans.angkatan,
                    angkatans.tahun as tahun_angkatan,
                    majors.inisial as inisial,
                    majors.nama as jurusan,
                    financing_categories.nama, 
                    count(payment_details.status) as banyak_tunggakan,
                    financing_periodes.nominal,
                    getNominalCicilan(payment_details.id) as cicilan_dibayar,
                    payments.jenis_pembayaran,
                    payments.persentase,
                    payment_details.status'))
                    ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                    ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('majors', 'majors.id', '=', 'students.major_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->orderBy('students.id')
                    ->groupBy('payment_details.payment_id')
                    ->where('payment_details.status','<>','Lunas')
                    ->get();
        }
        else
        {
            return DB::table('payment_details')
                    ->select(DB::raw('financing_categories.id as financing_category_id,
                    payment_details.id as detail_id, 
                    payment_details.payment_id,
                    payment_details.payment_periode_id,
                    angkatans.id as angkatan_id,
                    majors.id as major_id,
                    students.nama as nama_murid,
                    students.kelas,
                    angkatans.angkatan,
                    angkatans.tahun as tahun_angkatan,
                    majors.inisial as inisial,
                    majors.nama as jurusan,
                    financing_categories.nama, 
                    count(payment_details.status) as banyak_tunggakan,
                    financing_periodes.nominal,
                    getNominalCicilan(payment_details.id) as cicilan_dibayar,
                    payments.jenis_pembayaran,
                    payments.persentase,
                    payment_details.status'))
                    ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                    ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('majors', 'majors.id', '=', 'students.major_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->orderBy('financing_categories.id')
                    ->groupBy('payment_details.payment_id')
                    ->where('payment_details.status','<>','Lunas')
                    ->get();
        }
    }

    public static function nominalRekapTunggakan()
    {
        $mr = new MenuRekapController();
        $data = $mr->nominalRekapTunggakanArray();
        return $data[3];
    }

    public static function nominalRekapTunggakanArray()
    {
        $siswa = Student::join('angkatans', 'students.angkatan_id', '=', 'angkatans.id')->get();
        $p = new PerhitunganTunggakanSiswaController();
        $res = $p->process($siswa);
        return $res['summary'];
    }

    public function indexTunggakan($stat = 'Siswa')
    {
        $no = 1;
        $filter = '';
        $pilihan = '';
        if ($stat == 'Siswa')
        {
            $kelas = 'all';
            $jurusan = 'all';
            $angkatan = 'all';
            return view('export.menu.tunggakan2',compact('no','filter','pilihan', 'stat', 'kelas', 'jurusan', 'angkatan'));
        }
        else
        {
            $filter = 'all';
            $kelas = 'all';
            $jurusan = 'all';
            $stat = "Kategori";
            return view('export.menu.tunggakan_kategori2',compact('no','filter','pilihan', 'stat', 'kelas', 'jurusan', 'stat'));
        }
    }

    public function ajaxTunggakanMasterSiswa($stat = 'Siswa')
    {
        @set_time_limit(0);
        $search = isset($_GET['search']) ? $_GET['search'] : '';
        $order = isset($_GET['order']) ? $_GET['order'] : 'asc';
        $offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        
        $angkatan_ = isset($_GET['angkatan']) ? $_GET['angkatan'] : "all";
        $jurusan = isset($_GET['jurusan']) ? $_GET['jurusan'] : "all";

        $data = Student::join('angkatans', 'students.angkatan_id', '=', 'angkatans.id')
                ->orderBy('angkatans.angkatan',"desc")
                ->orderBy('students.major_id',"desc")
                ->orderBy('students.nama', $order);
        
        $total = Student::count();
        $stat = false;
        
        if ( $search != '' ) {
            $total = Student::where('nama', 'like', '%' .$search. '%')->count();
            $data->where('nama', 'like', '%' . $search . '%');
        } 

        if ($angkatan_ != "all") {
            $stat = true;
            $data->where('angkatan_id', $angkatan_);
        }

        if ($jurusan != "all") {
            $stat = true;
            $data->where('major_id', $jurusan);
        }
        
        if ($stat) {
            $total = $data->count();
        }

        $data->limit($limit)
            ->offset($offset);
        $siswa = $data->get();
        $res = $this->ptsc->process($siswa);
        return [
            'total' => $total,
            'rows'  => $res['row'],
        ];
    }

    public function ajaxTunggakanMasterKategori($stat = 'Kategori')
    {
        @set_time_limit(0);
        $search = isset($_GET['search']) ? $_GET['search'] : '';
        $order = isset($_GET['order']) ? $_GET['order'] : 'asc';
        $offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        
        $angkatan = isset($_GET['angkatan']) ? $_GET['angkatan'] : "all";
        $jurusan = isset($_GET['jurusan']) ? $_GET['jurusan'] : "all";

        $data = Student::join('angkatans', 'students.angkatan_id', '=', 'angkatans.id')
                ->limit($limit)
                ->offset($offset)
                ->orderBy('angkatans.angkatan',"desc")
                ->orderBy('students.major_id',"desc")
                ->orderBy('students.nama', $order);

        $total = Student::count();
        
        if ( $search != '' ) {
            $total = Student::where('nama', 'like', '%' .$search. '%')->count();
            $data->where('nama', 'like', '%' . $search . '%');
        } 

        if ($angkatan != "all") {
            $data->where('angkatan_id', $angkatan);
        }

        if ($jurusan != "all") {
            $data->where('jurusan_id', $jurusan);
        }

        $siswa = $data->get();
        $row = [];
        foreach ($siswa as $key => $value) {
            
            $angkatan = $value->angkatans->angkatan . " (" . $value->angkatans->tahun . ")";
            
            $temp = explode("_", $value->uname);
            $id = $temp[count($temp)-1];

            $item = array (
                "id" => $id,
                "nama" => $value->nama,
                "kelas" => $value->kelas,
                "jurusan" => $value->major->inisial,
                "angkatan" => $angkatan,
            );
            $data_tunggakan = $this->ptsc->siswa($id);
            $row[] = array_merge($item, $data_tunggakan);
        }
        
        return [
            'total' => $total,
            'rows'  => $row
        ];
    }

    public function ajaxTunggakanMaster($stat = 'Siswa')
    {
        @set_time_limit(0);
        $search = isset($_GET['search']) ? $_GET['search'] : '';
        $order = isset($_GET['order']) ? $_GET['order'] : 'asc';
        $offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        
        $kategori = isset($_GET['kategori']) ? $_GET['kategori'] : "all";
        $angkatan = isset($_GET['angkatan']) ? $_GET['angkatan'] : "all";
        $jurusan = isset($_GET['jurusan']) ? $_GET['jurusan'] : "all";

        if ($stat == 'Siswa') {
            $sort = isset($_GET['sort']) ? $_GET['sort'] : 'student_id';
        } else {
            $sort = isset($_GET['sort']) ? $_GET['sort'] : 'financing_category_id';
            $sort = ($sort!= 'financing_category_id') ? 'financing_category_id' : 'financing_category_id';
        }

        if ( $search=='' ) {
            // return $kategori . " " . $angkatan . " " . $jurusan . " ";
            if ($kategori == "all" && $angkatan == "all" && $jurusan == "all") {
                $data = Payment::offset($offset)
                    ->limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)->count();
            }
            if ($kategori != "all" && $angkatan == "all" && $jurusan == "all") {
                $data = Payment::offset($offset)
                    ->limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('financing_category_id', $kategori)
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('financing_category_id', $kategori)
                        ->count();
            }
            if ($kategori == "all" && $angkatan != "all" && $jurusan == "all") {
                $data = Payment::offset($offset)
                    ->limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('students.angkatan_id', $angkatan)
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('students.angkatan_id', $angkatan)
                        ->count();
            }
            if ($kategori == "all" && $angkatan == "all" && $jurusan != "all") {
                $data = Payment::offset($offset)
                    ->limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('students.major_id', $jurusan)
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('students.major_id', $jurusan)
                        ->count();
            }
            if ($kategori != "all" && $angkatan != "all" && $jurusan == "all") {
                $data = Payment::offset($offset)
                    ->limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('financing_category_id', $kategori)
                    ->where('students.angkatan_id', $angkatan)
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('financing_category_id', $kategori)
                        ->where('students.angkatan_id', $angkatan)
                        ->count();
            }
            if ($kategori != "all" && $angkatan == "all" && $jurusan != "all") {
                $data = Payment::offset($offset)
                    ->limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('financing_category_id', $kategori)
                    ->where('students.major_id', $jurusan)
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('financing_category_id', $kategori)
                        ->where('students.major_id', $jurusan)
                        ->count();
            }
            if ($kategori == "all" && $angkatan != "all" && $jurusan != "all") {
                $data = Payment::offset($offset)
                    ->limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('students.angkatan_id', $angkatan)
                    ->where('students.major_id', $jurusan)
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('students.angkatan_id', $angkatan)
                        ->where('students.major_id', $jurusan)
                        ->count();
            }

            if ($kategori != "all" && $angkatan != "all" && $jurusan != "all") {
                $data = Payment::offset($offset)
                    ->limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('financing_category_id', $kategori)
                    ->where('students.angkatan_id', $angkatan)
                    ->where('students.major_id', $jurusan)
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('financing_category_id', $kategori)
                        ->where('students.angkatan_id', $angkatan)
                        ->where('students.major_id', $jurusan)
                        ->count();
            }
        } else {
            if ($kategori != "all" && $angkatan != "all" && $jurusan != "all") {
                $data = Payment::offset($offset)
                    ->limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('financing_category_id', $kategori)
                    ->where('students.angkatan_id', $angkatan)
                    ->where('students.major_id', $jurusan)
                    ->where('students.nama', 'like', '%' .$search. '%')
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('financing_category_id', $kategori)
                        ->where('students.angkatan_id', $angkatan)
                        ->where('students.major_id', $jurusan)
                        ->where('students.nama', 'like', '%' .$search. '%')
                        ->count();
            }
            if ($kategori != "all" && $angkatan != "all" && $jurusan == "all") {
                $data = Payment::offset($offset)
                    ->limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('financing_category_id', $kategori)
                    ->where('students.angkatan_id', $angkatan)
                    ->where('students.nama', 'like', '%' .$search. '%')
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('financing_category_id', $kategori)
                        ->where('students.angkatan_id', $angkatan)
                        ->where('students.nama', 'like', '%' .$search. '%')
                        ->count();
            }
            if ($kategori != "all" && $angkatan == "all" && $jurusan != "all") {
                $data = Payment::offset($offset)
                    ->limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('financing_category_id', $kategori)
                    ->where('students.major_id', $jurusan)
                    ->where('students.nama', 'like', '%' .$search. '%')
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('financing_category_id', $kategori)
                        ->where('students.major_id', $jurusan)
                        ->where('students.nama', 'like', '%' .$search. '%')
                        ->count();
            }
            if ($kategori == "all" && $angkatan != "all" && $jurusan != "all") {
                $data = Payment::offset($offset)
                    ->limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('students.angkatan_id', $angkatan)
                    ->where('students.major_id', $jurusan)
                    ->where('students.nama', 'like', '%' .$search. '%')
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('students.angkatan_id', $angkatan)
                        ->where('students.major_id', $jurusan)
                        ->where('students.nama', 'like', '%' .$search. '%')
                        ->count();
            }
            if ($kategori == "all" && $angkatan == "all" && $jurusan != "all") {
                $data = Payment::offset($offset)
                    ->limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('students.major_id', $jurusan)
                    ->where('students.nama', 'like', '%' .$search. '%')
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('students.major_id', $jurusan)
                        ->where('students.nama', 'like', '%' .$search. '%')
                        ->count();
            }
            if ($kategori == "all" && $angkatan != "all" && $jurusan == "all") {
                $data = Payment::offset($offset)
                    ->limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('students.angkatan_id', $angkatan)
                    ->where('students.nama', 'like', '%' .$search. '%')
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('students.angkatan_id', $angkatan)
                        ->where('students.nama', 'like', '%' .$search. '%')
                        ->count();
            }
            if ($kategori != "all" && $angkatan == "all" && $jurusan == "all") {
                $data = Payment::offset($offset)
                    ->limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('financing_category_id', $kategori)
                    ->where('students.nama', 'like', '%' .$search. '%')
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('financing_category_id', $kategori)
                        ->where('students.nama', 'like', '%' .$search. '%')
                        ->count();
            }
            if ($kategori == "all" && $angkatan == "all" && $jurusan == "all") {
                $data = Payment::limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    ->offset($offset)
                    // ->orderBy('financing_categories.id',"asc")
                    // ->orderBy('students.angkatan_id',"asc")
                    ->where('students.nama', 'like', '%' .$search. '%')
                    // ->where('financing_categories.nama', 'like', '%' .$search. '%')
                    ->get();
                $total = Payment::join('students', 'students.id', '=', 'payments.student_id')
                        ->where('students.nama', 'like', '%' .$search. '%')
                        ->count();
            }
        }
        $rows  = [];
        $total_each = 0;
        foreach ($data as $i => $v){
            if ($v->category->jenis == "Bayar per Bulan") {
                $kelas_x = $v->detail[0]->periode->kelas_x;
                $kelas_xi = $v->detail[0]->periode->kelas_xi;
                $kelas_xii = $v->detail[0]->periode->kelas_xii;
                $besaran = $kelas_x*12 + $kelas_xi*12 + $kelas_xii*12;
                $potongan = 0;
                $terbayar = $v->detail->sum('nominal');
            }
            else
            {
                // echo "<pre>";
                // var_dump($v->student->nama);die;
                $besaran = $v->detail[0]->periode->nominal;
                if ( $v->jenis_potongan == "persentase")
                {
                    $potongan = (intval($v->persentase) / 100) * $besaran;
                }
                else
                {
                    $potongan = intval($v->nominal_potongan);
                }
                $terbayar = $v->detail[0]->cicilan->sum('nominal');
            }
            $sisa = $besaran - ($potongan + $terbayar);
            $angkatan = $v->student->angkatans->angkatan . " (" . $v->student->angkatans->tahun . ")";

            $total_each += $sisa;

            $item = (object) array (
                'id'        => $v->id,
                'nama'      => $v->student->nama,
                'kategori'  => $v->category->nama,
                'kelas'     => $v->student->kelas,
                'jurusan'   => $v->student->major->inisial,
                'angkatan'  => $angkatan,
                'besaran'   => $besaran,
                'potongan'  => $potongan,
                'terbayar'  => $terbayar,
                'sisa'      => $sisa,
                'total'      => $total_each,
            );
            if ($sisa != 0 ){
                $rows[] = $item;
            }
            
            if (isset($data[$i+1])) {
                if ($data[$i+1]->student->id != $v->student->id) {
                    $total_each = 0;
                }
            }
        }
        return [
            'total' => $total,
            'rows'  => $rows
        ];
    }

    public function parseDate($d)
    {
        $a = explode("/", $d);
        return $a[2] . "-" . $a[0] . "-" . $a[1];
    }

    public function parseDateFormat($d)
    {
        $a = explode("/", $d);
        return $a[1] . "-" . $a[0] . "-" . $a[2];
    }

    public function ajaxPemasukanMaster()
    {
        $search = isset($_GET['search']) ? $_GET['search'] : '';
        $order = isset($_GET['order']) ? $_GET['order'] : 'asc';
        $offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        
        $sort = isset($_GET['sort']) ? $_GET['sort'] : 'created_at';
        if ( $search=='' ) {
            if ($_GET['is_date'] == 1) {
                $start = date($this->parseDate($_GET['date_start']));
                $end = date($this->parseDate($_GET['date_end']));
                $data = Pencatatan::orderBy('pencatatans.updated_at', 'desc')
                    ->join('incomes','incomes.id','=','pencatatans.income_id')
                    ->limit($limit)
                    ->offset($offset)
                    ->whereBetween('pencatatans.created_at', [$start, $end])
                    ->where([
                        ['debit','<>','0'],
                    ])->get();
                $total = Pencatatan::orderBy('pencatatans.updated_at', 'desc')
                    ->join('incomes','incomes.id','=','pencatatans.income_id')
                    ->whereBetween('pencatatans.created_at', [$start, $end])
                    ->where([
                        ['debit','<>','0'],
                    ])->count();
            } else {
                $data = Pencatatan::orderBy('pencatatans.updated_at', 'desc')
                ->join('incomes','incomes.id','=','pencatatans.income_id')
                ->limit($limit)
                ->offset($offset)
                ->where([
                        ['debit','<>','0'],
                        ])->get();
                $total = Pencatatan::orderBy('pencatatans.updated_at', 'desc')
                    ->join('incomes','incomes.id','=','pencatatans.income_id')
                    ->where([
                        ['debit','<>','0'],
                    ])->count();
            }
        } else {
            $data = Pencatatan::orderBy('pencatatans.updated_at', 'desc')
                    ->join('incomes','incomes.id','=','pencatatans.income_id')
                    ->limit($limit)
                    ->offset($offset)
                    ->where('incomes.title', 'like', '%' .$search. '%')
                    ->where([
                        ['debit','<>','0'],
                    ])->get();
            $total = Pencatatan::orderBy('pencatatans.updated_at', 'desc')
                    ->join('incomes','incomes.id','=','pencatatans.income_id')
                    ->where('incomes.title', 'like', '%' .$search. '%')
                    ->where([
                        ['debit','<>','0'],
                    ])->count();
        }
        $rows  = [];
        $no = $offset + 1;
        foreach ($data as $i => $v){
            $url = "nota/{$v->foto}";
            $url = asset('').$url;
            $html = "<img src='".$url."' style='height:50' alt='foto nota kosong'/>";
            $temp = strtotime($v->created_at);
            $tanggal = date('j - M - Y', $temp);
            $item = (object) array (
                'id'            => $v->id,
                'no'            => $no++,
                'foto'          => $html,
                'tanggal'       => $tanggal,
                'description'   => $v->description,
                'nominal'       => $v->debit,
            );
            $rows[] = $item;
        }
        return [
            'total' => $total,
            'rows'  => $rows
        ];
    }

    public function ajaxPemasukanTotal()
    {
        return Pencatatan::orderBy('pencatatans.updated_at', 'desc')
                    ->join('incomes','incomes.id','=','pencatatans.income_id')
                    ->where([
                        ['debit','<>','0'],
                    ])->sum('nominal');
    }

    public function indexTunggakanFilter(Request $request, $stat = 'Siswa')
    {
        $no = 1;
        $filter = $request->filter;
        $pilihan = '';
        
        $kelas = $request->kelas;
        $jurusan = $request->jurusan;
        $angkatan = $request->angkatan;
        $filter = $request->filter;
        if ($stat == 'Siswa')
        {
            if ($kelas == 'all' && $jurusan == 'all' && $angkatan == 'all')
            {
                $datas = DB::table('payment_details')
                        ->select(DB::raw('financing_categories.id as financing_category_id,
                        payment_details.id as detail_id, 
                        payment_details.payment_id,
                        payment_details.payment_periode_id,
                        angkatans.id as angkatan_id,
                        majors.id as major_id,
                        students.nama as nama_murid,
                        students.kelas,
                        angkatans.angkatan,
                        angkatans.tahun as tahun_angkatan,
                        majors.inisial as inisial,
                        majors.nama as jurusan,
                        financing_categories.nama, 
                        count(payment_details.status) as banyak_tunggakan,
                        financing_periodes.nominal,
                        getNominalCicilan(payment_details.id) as cicilan_dibayar,
                        payments.jenis_pembayaran,
                    payments.persentase,
                        payment_details.status'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                        ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->join('majors', 'majors.id', '=', 'students.major_id')
                        ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                        ->orderBy('students.id')
                        ->groupBy('payment_details.payment_id')
                        ->where('payment_details.status','<>','Lunas')
                        ->get();
            }
            else if ($kelas != 'all' && $jurusan == 'all' && $angkatan == 'all')
            {
                $datas = DB::table('payment_details')
                        ->select(DB::raw('financing_categories.id as financing_category_id,
                        payment_details.id as detail_id, 
                        payment_details.payment_id,
                        payment_details.payment_periode_id,
                        angkatans.id as angkatan_id,
                        majors.id as major_id,
                        students.nama as nama_murid,
                        students.kelas,
                        angkatans.angkatan,
                        angkatans.tahun as tahun_angkatan,
                        majors.inisial as inisial,
                        majors.nama as jurusan,
                        financing_categories.nama, 
                        count(payment_details.status) as banyak_tunggakan,
                        financing_periodes.nominal,
                        getNominalCicilan(payment_details.id) as cicilan_dibayar,
                        payments.jenis_pembayaran,
                    payments.persentase,
                        payment_details.status'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                        ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->join('majors', 'majors.id', '=', 'students.major_id')
                        ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                        ->orderBy('students.id')
                        ->groupBy('payment_details.payment_id')
                        ->where('payment_details.status','<>','Lunas')
                        ->where('students.kelas','=', $kelas)
                        ->get();
            }
            else if ($kelas == 'all' && $jurusan != 'all' && $angkatan == 'all')
            {
                $datas = DB::table('payment_details')
                        ->select(DB::raw('financing_categories.id as financing_category_id,
                        payment_details.id as detail_id, 
                        payment_details.payment_id,
                        payment_details.payment_periode_id,
                        angkatans.id as angkatan_id,
                        majors.id as major_id,
                        students.nama as nama_murid,
                        students.kelas,
                        angkatans.angkatan,
                        angkatans.tahun as tahun_angkatan,
                        majors.inisial as inisial,
                        majors.nama as jurusan,
                        financing_categories.nama, 
                        count(payment_details.status) as banyak_tunggakan,
                        financing_periodes.nominal,
                        getNominalCicilan(payment_details.id) as cicilan_dibayar,
                        payments.jenis_pembayaran,
                    payments.persentase,
                        payment_details.status'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                        ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->join('majors', 'majors.id', '=', 'students.major_id')
                        ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                        ->orderBy('students.id')
                        ->groupBy('payment_details.payment_id')
                        ->where('payment_details.status','<>','Lunas')
                        ->where('majors.id','=', $jurusan)
                        ->get();
            }
            else if ($kelas == 'all' && $jurusan == 'all' && $angkatan != 'all')
            {
                $datas = DB::table('payment_details')
                        ->select(DB::raw('financing_categories.id as financing_category_id,
                        payment_details.id as detail_id, 
                        payment_details.payment_id,
                        payment_details.payment_periode_id,
                        angkatans.id as angkatan_id,
                        majors.id as major_id,
                        students.nama as nama_murid,
                        students.kelas,
                        angkatans.angkatan,
                        angkatans.tahun as tahun_angkatan,
                        majors.inisial as inisial,
                        majors.nama as jurusan,
                        financing_categories.nama, 
                        count(payment_details.status) as banyak_tunggakan,
                        financing_periodes.nominal,
                        getNominalCicilan(payment_details.id) as cicilan_dibayar,
                        payments.jenis_pembayaran,
                    payments.persentase,
                        payment_details.status'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                        ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->join('majors', 'majors.id', '=', 'students.major_id')
                        ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                        ->orderBy('students.id')
                        ->groupBy('payment_details.payment_id')
                        ->where('payment_details.status','<>','Lunas')
                        ->where('angkatans.id','=', $angkatan)
                        ->get();
            }
            else if ($kelas != 'all' && $jurusan != 'all' && $angkatan == 'all')
            {
                $datas = DB::table('payment_details')
                        ->select(DB::raw('financing_categories.id as financing_category_id,
                        payment_details.id as detail_id, 
                        payment_details.payment_id,
                        payment_details.payment_periode_id,
                        angkatans.id as angkatan_id,
                        majors.id as major_id,
                        students.nama as nama_murid,
                        students.kelas,
                        angkatans.angkatan,
                        angkatans.tahun as tahun_angkatan,
                        majors.inisial as inisial,
                        majors.nama as jurusan,
                        financing_categories.nama, 
                        count(payment_details.status) as banyak_tunggakan,
                        financing_periodes.nominal,
                        getNominalCicilan(payment_details.id) as cicilan_dibayar,
                        payments.jenis_pembayaran,
                    payments.persentase,
                        payment_details.status'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                        ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->join('majors', 'majors.id', '=', 'students.major_id')
                        ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                        ->orderBy('students.id')
                        ->groupBy('payment_details.payment_id')
                        ->where('payment_details.status','<>','Lunas')
                        ->where('students.kelas','=', $kelas)
                        ->where('majors.id','=', $jurusan)
                        ->get();
            }
            else if ($kelas != 'all' && $jurusan == 'all' && $angkatan != 'all')
            {
                $datas = DB::table('payment_details')
                        ->select(DB::raw('financing_categories.id as financing_category_id,
                        payment_details.id as detail_id, 
                        payment_details.payment_id,
                        payment_details.payment_periode_id,
                        angkatans.id as angkatan_id,
                        majors.id as major_id,
                        students.nama as nama_murid,
                        students.kelas,
                        angkatans.angkatan,
                        angkatans.tahun as tahun_angkatan,
                        majors.inisial as inisial,
                        majors.nama as jurusan,
                        financing_categories.nama, 
                        count(payment_details.status) as banyak_tunggakan,
                        financing_periodes.nominal,
                        getNominalCicilan(payment_details.id) as cicilan_dibayar,
                        payments.jenis_pembayaran,
                    payments.persentase,
                        payment_details.status'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                        ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->join('majors', 'majors.id', '=', 'students.major_id')
                        ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                        ->orderBy('students.id')
                        ->groupBy('payment_details.payment_id')
                        ->where('payment_details.status','<>','Lunas')
                        ->where('angkatans.id','=', $angkatan)
                        ->where('students.kelas','=', $kelas)
                        ->get();
            }
            else if ($kelas == 'all' && $jurusan != 'all' && $angkatan != 'all')
            {
                $datas = DB::table('payment_details')
                        ->select(DB::raw('financing_categories.id as financing_category_id,
                        payment_details.id as detail_id, 
                        payment_details.payment_id,
                        payment_details.payment_periode_id,
                        angkatans.id as angkatan_id,
                        majors.id as major_id,
                        students.nama as nama_murid,
                        students.kelas,
                        angkatans.angkatan,
                        angkatans.tahun as tahun_angkatan,
                        majors.inisial as inisial,
                        majors.nama as jurusan,
                        financing_categories.nama, 
                        count(payment_details.status) as banyak_tunggakan,
                        financing_periodes.nominal,
                        getNominalCicilan(payment_details.id) as cicilan_dibayar,
                        payments.jenis_pembayaran,
                    payments.persentase,
                        payment_details.status'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                        ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->join('majors', 'majors.id', '=', 'students.major_id')
                        ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                        ->orderBy('students.id')
                        ->groupBy('payment_details.payment_id')
                        ->where('payment_details.status','<>','Lunas')
                        ->where('angkatans.id','=', $angkatan)
                        ->where('majors.id','=', $jurusan)
                        ->get();
            }
            else
            {
                $datas = DB::table('payment_details')
                        ->select(DB::raw('financing_categories.id as financing_category_id,
                        payment_details.id as detail_id, 
                        payment_details.payment_id,
                        payment_details.payment_periode_id,
                        angkatans.id as angkatan_id,
                        majors.id as major_id,
                        students.nama as nama_murid,
                        students.kelas,
                        angkatans.angkatan,
                        angkatans.tahun as tahun_angkatan,
                        majors.inisial as inisial,
                        majors.nama as jurusan,
                        financing_categories.nama, 
                        count(payment_details.status) as banyak_tunggakan,
                        financing_periodes.nominal,
                        getNominalCicilan(payment_details.id) as cicilan_dibayar,
                        payments.jenis_pembayaran,
                    payments.persentase,
                        payment_details.status'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                        ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->join('majors', 'majors.id', '=', 'students.major_id')
                        ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                        ->orderBy('students.id')
                        ->groupBy('payment_details.payment_id')
                        ->where('payment_details.status','<>','Lunas')
                        ->where('angkatans.id','=', $angkatan)
                        ->where('students.kelas','=', $kelas)
                        ->where('majors.id','=', $jurusan)
                        ->get();
            }

            return view('export.menu.tunggakan',compact('no','datas','filter','pilihan', 'stat', 'kelas', 'jurusan', 'angkatan'));
        }
        else
        {
            if ($kelas == 'all' && $jurusan == 'all' && $filter == 'all')
                {
                    $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                            payments.persentase,
                            payments.jenis_potongan,
                            payments.nominal_potongan,  
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('financing_categories.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->get();
                }
                else if ($kelas != 'all' && $jurusan == 'all' && $filter == 'all')
                {
                    
                    $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                            payments.persentase,
                            payments.jenis_potongan,
                            payments.nominal_potongan,
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('financing_categories.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->where('students.kelas', $request->kelas)
                            ->get();
                }
                else if ($kelas == 'all' && $jurusan != 'all' && $filter == 'all')
                {
                    
                    $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                            payments.persentase,
                            payments.jenis_potongan,
                            payments.nominal_potongan,
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('financing_categories.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->where('majors.id', $request->jurusan)
                            ->get();
                }
                else if ($kelas == 'all' && $jurusan == 'all' && $filter != 'all')
                {
                    $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                        payments.persentase,
                        payments.jenis_potongan,
                        payments.nominal_potongan,
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('financing_categories.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->where('financing_categories.id','=', $request->filter)
                            ->get();
                }
                else if ($kelas != 'all' && $jurusan != 'all' && $filter == 'all')
                {
                    
                    $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                            payments.persentase,
                            payments.jenis_potongan,
                            payments.nominal_potongan,
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('financing_categories.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->where('students.kelas', $request->kelas)
                            ->where('majors.id', $request->jurusan)
                            ->get();
                }
                else if ($kelas != 'all' && $jurusan == 'all' && $filter != 'all')
                {
                    
                    $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                            payments.persentase,
                            payments.jenis_potongan,
                            payments.nominal_potongan,
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('financing_categories.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->where('financing_categories.id', $request->filter)
                            ->where('students.kelas', $request->kelas)
                            ->get();
                }
                else if ($kelas == 'all' && $jurusan != 'all' && $filter != 'all')
                {
                    
                    $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                            payments.persentase,
                            payments.jenis_potongan,
                            payments.nominal_potongan,
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('financing_categories.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->where('financing_categories.id', $request->filter)
                            ->where('majors.id', $request->jurusan)
                            ->get();
                }
                else
                {
                    $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                            payments.persentase,
                            payments.jenis_potongan,
                            payments.nominal_potongan,
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('financing_categories.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->where('financing_categories.id', $request->filter)
                            ->where('students.kelas', $request->kelas)
                            ->where('majors.id', $request->jurusan)
                            ->get();
                }
            return $datas;
            // return view('export.menu.tunggakan_kategori2',compact('no','datas','filter','pilihan', 'stat', 'kelas', 'jurusan', 'stat'));
            // return view('export.menu.tunggakan_kategori',compact('no','datas','filter','pilihan', 'stat'));
        }
    }

    //Tunggakan
    public function tunggakan(Request $request)
    {
        ini_set('memory_limit','512MB').
        $stat = $request->stat;
        $no = 1;
        $filter = $request->filter;
        $pilihan = '';

        $t = now();
        $t = explode(" ", $t);
        $t = explode("-", $t[0]);
        $tanggal = "{$t[2]} {$t[1]} {$t[0]}";
        $no=1;       
        $kelas = $request->kelas;
        $jurusan = $request->jurusan;
        $angkatan = $request->angkatan;
        $filter = $request->filter;

        $user= Auth::user()->nama;
        $rincian = "Tunggakan";
        $title = "Laporan Tunggakan";

        if ($stat == 'Siswa')
        {
            if ($request->check_options != "[]" && $request->check_options != "")
            {
                $temp_id = json_decode($request->check_options);
                $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                            payments.persentase,
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('students.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->whereIn('payment_details.id', $temp_id)
                            ->get();
                            
                $pdf = PDF::loadView('export.rekap_tunggakan_siswa',compact('no','title','datas'));
                $pdf->setPaper('A4', 'landscape');
                // $pdf->setPaper('A4', 'potrait');
                return $pdf->stream();
            }
     
            if (!$request->keyword)
            {
                if ($kelas == 'all' && $jurusan == 'all' && $angkatan == 'all')
                {
                    $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                            payments.persentase,
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('students.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->get();
                }
                else if ($kelas != 'all' && $jurusan == 'all' && $angkatan == 'all')
                {
                    $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                        payments.persentase,
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('students.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->where('students.kelas','=', $kelas)
                            ->get();
                }
                else if ($kelas == 'all' && $jurusan != 'all' && $angkatan == 'all')
                {
                    $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                        payments.persentase,
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('students.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->where('majors.id','=', $jurusan)
                            ->get();
                }
                else if ($kelas == 'all' && $jurusan == 'all' && $angkatan != 'all')
                {
                    $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                        payments.persentase,
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('students.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->where('angkatans.id','=', $angkatan)
                            ->get();
                }
                else if ($kelas != 'all' && $jurusan != 'all' && $angkatan == 'all')
                {
                    $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                        payments.persentase,
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('students.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->where('students.kelas','=', $kelas)
                            ->where('majors.id','=', $jurusan)
                            ->get();
                }
                else if ($kelas != 'all' && $jurusan == 'all' && $angkatan != 'all')
                {
                    $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                        payments.persentase,
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('students.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->where('angkatans.id','=', $angkatan)
                            ->where('students.kelas','=', $kelas)
                            ->get();
                }
                else if ($kelas == 'all' && $jurusan != 'all' && $angkatan != 'all')
                {
                    $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                        payments.persentase,
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('students.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->where('angkatans.id','=', $angkatan)
                            ->where('majors.id','=', $jurusan)
                            ->get();
                }
                else
                {
                    $datas = DB::table('payment_details')
                            ->select(DB::raw('financing_categories.id as financing_category_id,
                            payment_details.id, 
                            payment_details.payment_id,
                            payment_details.payment_periode_id,
                            angkatans.id as angkatan_id,
                            majors.id as major_id,
                            students.nama as nama_murid,
                            students.kelas,
                            angkatans.angkatan,
                            angkatans.tahun as tahun_angkatan,
                            majors.inisial as inisial,
                            majors.nama as jurusan,
                            financing_categories.nama, 
                            count(payment_details.status) as banyak_tunggakan,
                            financing_periodes.nominal,
                            getNominalCicilan(payment_details.id) as cicilan_dibayar,
                            payments.jenis_pembayaran,
                        payments.persentase,
                            payment_details.status'))
                            ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                            ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                            ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                            ->join('students', 'students.id', '=', 'payments.student_id')
                            ->join('majors', 'majors.id', '=', 'students.major_id')
                            ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                            ->orderBy('students.id')
                            ->groupBy('payment_details.payment_id')
                            ->where('payment_details.status','<>','Lunas')
                            ->where('angkatans.id','=', $angkatan)
                            ->where('students.kelas','=', $kelas)
                            ->where('majors.id','=', $jurusan)
                            ->get();
                }
            }
            else
            {
                $datas = DB::table('payment_details')
                        ->select(DB::raw('financing_categories.id as financing_category_id,
                        payment_details.id, 
                        payment_details.payment_id,
                        payment_details.payment_periode_id,
                        angkatans.id as angkatan_id,
                        majors.id as major_id,
                        students.nama as nama_murid,
                        students.kelas,
                        angkatans.angkatan,
                        angkatans.tahun as tahun_angkatan,
                        majors.inisial as inisial,
                        majors.nama as jurusan,
                        financing_categories.nama, 
                        count(payment_details.status) as banyak_tunggakan,
                        financing_periodes.nominal,
                        getNominalCicilan(payment_details.id) as cicilan_dibayar,
                        payments.jenis_pembayaran,
                        payments.persentase,
                        payment_details.status'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                        ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->join('majors', 'majors.id', '=', 'students.major_id')
                        ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                        ->orderBy('financing_categories.id')
                        ->groupBy('payment_details.payment_id')
                        ->where('payment_details.status','<>','Lunas')
                        ->where('students.nama', 'like', '%' . $request->keyword . '%')
                        ->get();
            }
            $pdf = PDF::loadView('export.rekap_tunggakan_siswa',compact('no','title','datas'));
            $pdf->setPaper('A4', 'landscape');
            // $pdf->setPaper('A4', 'potrait');
            return $pdf->stream();
        }
        else
        {
            @set_time_limit(0);
            $search = isset($_POST['keyword']) ? $_POST['keyword'] : '';
        $order = isset($_POST['order']) ? $_POST['order'] : 'asc';
        $offset = isset($_POST['offset']) ? $_POST['offset'] : 0;
        $limit = isset($_POST['limit']) ? $_POST['limit'] : 10;
        
        $kategori = isset($_POST['filter']) ? $_POST['filter'] : "all";
        $angkatan = isset($_POST['kelas']) ? $_POST['kelas'] : "all";
        $jurusan = isset($_POST['jurusan']) ? $_POST['jurusan'] : "all";

        $sort = isset($_POST['sort']) ? $_POST['sort'] : 'financing_category_id';
        $sort = ($sort!= 'financing_category_id') ? 'financing_category_id' : 'financing_category_id';
        
        
        if ( $search=='' ) {
            // return $_POST;
            if ($kategori == "all" && $angkatan == "all" && $jurusan == "all") {
                // $total = Payment::where('payments.status_lunas', 0)->count();
                $total = 10;
                $param = (object) array(
                    'kategori' => $kategori,
                    'angkatan' => $angkatan,
                    'jurusan' => $jurusan,
                    'total' => $total,
                );
                $data = [];
                // $data = Payment::join('students', 'students.id', '=', 'payments.student_id')
                //     ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                //     ->orderBy('angkatans.angkatan',"desc")
                //     ->orderBy('students.major_id',"desc")
                //     ->orderBy('students.nama',"asc")
                //     ->where('payments.status_lunas', 0)
                //     ->limit(100)
                //     ->get();
                return view('export.rekap_tunggakan_kategori', compact('no','title','data', 'param'));
                    // ->chunk(100, function($data){
                    //     foreach ($data as $key => $value) {
                    //         echo $value;
                    //     }
                    // });
                // return $data;
                // return $total;
                // return $kategori . " " . $angkatan . " " . $jurusan . " ";
            }
            else if ($kategori != "all" && $angkatan == "all" && $jurusan == "all") {
                $data = Payment::addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('financing_category_id', $kategori)
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('financing_category_id', $kategori)
                        ->count();
            }
            else if ($kategori == "all" && $angkatan != "all" && $jurusan == "all") {
                $data = Payment::addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('students.angkatan_id', $angkatan)
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('students.angkatan_id', $angkatan)
                        ->count();
            }
            else if ($kategori == "all" && $angkatan == "all" && $jurusan != "all") {
                $data = Payment::addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('students.major_id', $jurusan)
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('students.major_id', $jurusan)
                        ->count();
            }
            else if ($kategori != "all" && $angkatan != "all" && $jurusan == "all") {
                $data = Payment::addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('financing_category_id', $kategori)
                    ->where('students.angkatan_id', $angkatan)
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('financing_category_id', $kategori)
                        ->where('students.angkatan_id', $angkatan)
                        ->count();
            }
            else if ($kategori != "all" && $angkatan == "all" && $jurusan != "all") {
                $data = Payment::addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('financing_category_id', $kategori)
                    ->where('students.major_id', $jurusan)
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('financing_category_id', $kategori)
                        ->where('students.major_id', $jurusan)
                        ->count();
            }
            else if ($kategori == "all" && $angkatan != "all" && $jurusan != "all") {
                $data = Payment::addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('students.angkatan_id', $angkatan)
                    ->where('students.major_id', $jurusan)
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('students.angkatan_id', $angkatan)
                        ->where('students.major_id', $jurusan)
                        ->count();
            }

            else if ($kategori != "all" && $angkatan != "all" && $jurusan != "all") {
                $data = Payment::addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('financing_category_id', $kategori)
                    ->where('students.angkatan_id', $angkatan)
                    ->where('students.major_id', $jurusan)
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('financing_category_id', $kategori)
                        ->where('students.angkatan_id', $angkatan)
                        ->where('students.major_id', $jurusan)
                        ->count();
            }
        } else {
            if ($kategori != "all" && $angkatan != "all" && $jurusan != "all") {
                $data = Payment::addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('financing_category_id', $kategori)
                    ->where('students.angkatan_id', $angkatan)
                    ->where('students.major_id', $jurusan)
                    ->where('students.nama', 'like', '%' .$search. '%')
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('financing_category_id', $kategori)
                        ->where('students.angkatan_id', $angkatan)
                        ->where('students.major_id', $jurusan)
                        ->where('students.nama', 'like', '%' .$search. '%')
                        ->count();
            }
            if ($kategori != "all" && $angkatan != "all" && $jurusan == "all") {
                $data = Payment::addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('financing_category_id', $kategori)
                    ->where('students.angkatan_id', $angkatan)
                    ->where('students.nama', 'like', '%' .$search. '%')
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('financing_category_id', $kategori)
                        ->where('students.angkatan_id', $angkatan)
                        ->where('students.nama', 'like', '%' .$search. '%')
                        ->count();
            }
            if ($kategori != "all" && $angkatan == "all" && $jurusan != "all") {
                $data = Payment::addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('financing_category_id', $kategori)
                    ->where('students.major_id', $jurusan)
                    ->where('students.nama', 'like', '%' .$search. '%')
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('financing_category_id', $kategori)
                        ->where('students.major_id', $jurusan)
                        ->where('students.nama', 'like', '%' .$search. '%')
                        ->count();
            }
            if ($kategori == "all" && $angkatan != "all" && $jurusan != "all") {
                $data = Payment::addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('students.angkatan_id', $angkatan)
                    ->where('students.major_id', $jurusan)
                    ->where('students.nama', 'like', '%' .$search. '%')
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('students.angkatan_id', $angkatan)
                        ->where('students.major_id', $jurusan)
                        ->where('students.nama', 'like', '%' .$search. '%')
                        ->count();
            }
            if ($kategori == "all" && $angkatan == "all" && $jurusan != "all") {
                $data = Payment::addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('students.major_id', $jurusan)
                    ->where('students.nama', 'like', '%' .$search. '%')
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('students.major_id', $jurusan)
                        ->where('students.nama', 'like', '%' .$search. '%')
                        ->count();
            }
            if ($kategori == "all" && $angkatan != "all" && $jurusan == "all") {
                $data = Payment::addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('students.angkatan_id', $angkatan)
                    ->where('students.nama', 'like', '%' .$search. '%')
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('students.angkatan_id', $angkatan)
                        ->where('students.nama', 'like', '%' .$search. '%')
                        ->count();
            }
            if ($kategori != "all" && $angkatan == "all" && $jurusan == "all") {
                $data = Payment::addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    // ->orderBy($sort, $order)
                    ->orderBy('angkatans.angkatan',"desc")
                    ->orderBy('students.major_id',"desc")
                    ->orderBy('students.nama',"asc")
                    ->where('payments.status_lunas', 0)
                    ->where('financing_category_id', $kategori)
                    ->where('students.nama', 'like', '%' .$search. '%')
                    ->get();
                $total = Payment::where('payments.status_lunas', 0)
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->where('financing_category_id', $kategori)
                        ->where('students.nama', 'like', '%' .$search. '%')
                        ->count();
            }
            if ($kategori == "all" && $angkatan == "all" && $jurusan == "all") {
                $data = Payment::limit($limit)
                    ->addSelect(DB::raw("
                    students.id as student_id,
                    payments.id as id,
                    students.angkatan_id,
                    financing_categories.id as financing_category_id,
                    financing_categories.jenis
                    "))
                    ->join('students', 'students.id', '=', 'payments.student_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    ->offset($offset)
                    // ->orderBy('financing_categories.id',"asc")
                    // ->orderBy('students.angkatan_id',"asc")
                    ->where('students.nama', 'like', '%' .$search. '%')
                    // ->where('financing_categories.nama', 'like', '%' .$search. '%')
                    ->get();
                $total = Payment::join('students', 'students.id', '=', 'payments.student_id')
                        ->where('students.nama', 'like', '%' .$search. '%')
                        ->count();
            }
        }
            return view('export.rekap_tunggakan_kategori', compact('no','title','data'));
            // $pdf = PDF::loadView('export.rekap_tunggakan_kategori',compact('no','title','datas'));
            // $pdf->setPaper('A4', 'landscape');
            // // $pdf->setPaper('A4', 'potrait');
            
            // return $pdf->stream();
        }
    }

    public function ajaxTunggakanView($keyword = '')
    {
        $total = [0,0,0,0];
        $datas = DB::table('payment_details')
                        ->select(DB::raw('financing_categories.id as financing_category_id,
                        payment_details.id, 
                        payment_details.payment_id,
                        payment_details.payment_periode_id,
                        angkatans.id as angkatan_id,
                        majors.id as major_id,
                        students.nama as nama_murid,
                        students.kelas,
                        angkatans.angkatan,
                        angkatans.tahun as tahun_angkatan,
                        majors.inisial as inisial,
                        majors.nama as jurusan,
                        financing_categories.nama, 
                        count(payment_details.status) as banyak_tunggakan,
                        financing_periodes.nominal,
                        getNominalCicilan(payment_details.id) as cicilan_dibayar,
                        payments.jenis_pembayaran,
                        payments.persentase,
                        payment_details.status'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                        ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->join('majors', 'majors.id', '=', 'students.major_id')
                        ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                        ->orderBy('financing_categories.id')
                        ->groupBy('payment_details.payment_id')
                        ->where('payment_details.status','<>','Lunas')
                        ->where('students.nama', 'like', '%' . $keyword . '%')
                        ->get();
        foreach ($datas as $data) {
            $bulan_spp = 36;
            if ($data->nama == 'SPP')
            {
                $besaran = $bulan_spp * (int) $data->nominal;
                $terbayar = ($bulan_spp - (int) $data->banyak_tunggakan) * (int)$data->nominal;
                $potongan = 0;
            }
            else
            {
                $besaran = (int) $data->nominal;
                $terbayar = $data->cicilan_dibayar == null ? 0 : (int) $data->cicilan_dibayar;
                $potongan = (int) (((int)$data->persentase * (int) $data->nominal)/100);
            }
            $sisa = $besaran - ( $terbayar + $potongan );

            $total[0] += (int) $besaran;
            $total[1] += (int) $potongan;
            $total[2] += (int) $terbayar;
            $total[3] += (int) $sisa;
        }
        $arr = array(
            'besaran' => number_format($total[0],0,',','.'),
            'potongan' => number_format($total[1],0,',','.'),
            'terbayar' => number_format($total[2],0,',','.'),
            'sisa' => number_format($total[3],0,',','.'),
        );
        return json_encode($arr);
    }

    public function ajaxTunggakanKategoriView($keyword = '')
    {
        $total = [0,0,0,0];
        $datas = DB::table('payment_details')
                        ->select(DB::raw('financing_categories.id as financing_category_id,
                        payment_details.id, 
                        payment_details.payment_id,
                        payment_details.payment_periode_id,
                        angkatans.id as angkatan_id,
                        majors.id as major_id,
                        students.nama as nama_murid,
                        students.kelas,
                        angkatans.angkatan,
                        angkatans.tahun as tahun_angkatan,
                        majors.inisial as inisial,
                        majors.nama as jurusan,
                        financing_categories.nama, 
                        count(payment_details.status) as banyak_tunggakan,
                        financing_periodes.nominal,
                        getNominalCicilan(payment_details.id) as cicilan_dibayar,
                        payments.jenis_pembayaran,
                        payments.persentase,
                        payment_details.status'))   
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                        ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->join('majors', 'majors.id', '=', 'students.major_id')
                        ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                        ->orderBy('financing_categories.id')
                        ->groupBy('payment_details.payment_id')
                        ->where('payment_details.status','<>','Lunas')
                        ->orWhere('financing_categories.nama', 'like', '%' . $keyword . '%')
                        ->get();
        foreach ($datas as $data) {
            $bulan_spp = 36;
            if ($data->nama == 'SPP')
            {
                $besaran = $bulan_spp * (int) $data->nominal;
                $terbayar = ($bulan_spp - (int) $data->banyak_tunggakan) * (int)$data->nominal;
                $potongan = 0;
            }
            else
            {
                $besaran = (int) $data->nominal;
                $terbayar = $data->cicilan_dibayar == null ? 0 : (int) $data->cicilan_dibayar;
                $potongan = (int) (((int)$data->persentase * (int) $data->nominal)/100);
            }
            $sisa = $besaran - ( $terbayar + $potongan );

            $total[0] += (int) $besaran;
            $total[1] += (int) $potongan;
            $total[2] += (int) $terbayar;
            $total[3] += (int) $sisa;
        }
        $arr = array(
            'besaran' => number_format($total[0],0,',','.'),
            'potongan' => number_format($total[1],0,',','.'),
            'terbayar' => number_format($total[2],0,',','.'),
            'sisa' => number_format($total[3],0,',','.'),
        );
        return json_encode($arr);
    }

    public function ajaxTunggakanSiswa($id)
    {
        return DB::table('payment_details')
                        ->select(DB::raw('financing_categories.id as financing_category_id,
                        payment_details.id, 
                        payment_details.payment_id,
                        payment_details.payment_periode_id,
                        angkatans.id as angkatan_id,
                        majors.id as major_id,
                        students.nama as nama_murid,
                        students.kelas,
                        angkatans.angkatan,
                        angkatans.tahun as tahun_angkatan,
                        majors.inisial as inisial,
                        majors.nama as jurusan,
                        financing_categories.nama, 
                        count(payment_details.status) as banyak_tunggakan,
                        financing_periodes.nominal,
                        getNominalCicilan(payment_details.id) as cicilan_dibayar,
                        payments.jenis_pembayaran,
                        payments.persentase,
                        payment_details.status'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                        ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->join('majors', 'majors.id', '=', 'students.major_id')
                        ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                        ->orderBy('financing_categories.id')
                        ->groupBy('payment_details.payment_id')
                        ->where('payment_details.status','<>','Lunas')
                        ->where('students.id','=',$id)
                        ->get();
    }

    public function ajaxTunggakan($stat = 'Siswa', $filter = 'Kelas')
    {
        if ($stat == 'Siswa')
        {
            if ($filter == 'Kelas')
            {
                return DB::table('payment_details')
                        ->select(DB::raw('students.kelas as kategori, students.kelas as kategori_value'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->groupBy('students.kelas')
                        ->where('payment_details.status','<>','Lunas')
                        ->get();
                    }
            else if ($filter == 'Jurusan')
            {
                return DB::table('payment_details')
                        ->select(DB::raw('majors.nama as kategori, majors.id as kategori_value'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->join('majors', 'majors.id', '=', 'students.major_id')
                        ->groupBy('majors.id')
                        ->orderBy('majors.nama', 'asc')
                        ->where('payment_details.status','<>','Lunas')
                        ->get();
            }
            else
            {
                return DB::table('payment_details')
                        ->select(DB::raw('angkatans.angkatan as kategori, angkatans.tahun as tahun, angkatans.id as kategori_value'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->join('angkatans', 'angkatans.id', '=', 'students.angkatan_id')
                        ->groupBy('angkatans.id')
                        ->orderBy('angkatans.angkatan', 'desc')
                        ->where('payment_details.status','<>','Lunas')
                        ->get();
            }
        }
        else
        {
            if ($filter == 'Kelas')
            {
                return DB::table('payment_details')
                        ->select(DB::raw('students.kelas as kategori, students.kelas as kategori_value'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->groupBy('students.kelas')
                        ->where('payment_details.status','<>','Lunas')
                        ->get();
                    }
            else if ($filter == 'Jurusan')
            {
                return DB::table('payment_details')
                        ->select(DB::raw('majors.nama as kategori, majors.id as kategori_value'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                        ->join('students', 'students.id', '=', 'payments.student_id')
                        ->join('majors', 'majors.id', '=', 'students.major_id')
                        ->groupBy('majors.id')
                        ->orderBy('financing_categories.id')
                        ->where('payment_details.status','<>','Lunas')
                        ->get();
            }
            else
            {
                return DB::table('payment_details')
                        ->select(DB::raw('financing_categories.id as kategori_value, financing_categories.nama as kategori'))
                        ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                        ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                        ->orderBy('financing_categories.id')
                        ->groupBy('financing_categories.id')
                        ->where('payment_details.status','<>','Lunas')
                        ->get();
            }
        }
    }

    public function indexBB()
    {
        $no = 1;
        $datas = Pencatatan::whereYear('created_at', date("Y"))->get();
        $filter = '';
        $pilihan = '';
        return view('export.menu.buku_besar',compact('no','datas','filter','pilihan'));
    }

    public function indexBukuBesarFilter(Request $request)
    {
        $no = 1;
        switch ($request->filter) {
            case 'harian':
                //Query Filter Harian
                $datas = Pencatatan::orderBy('pencatatans.updated_at', 'desc')
                    ->whereDate('pencatatans.created_at', $request->pilihan)
                    ->get();
                break;
            case 'bulanan':
                //Query Filter Bulanan
                $temp = $request->pilihan;
                $_temp = explode('-', $temp);
                $datas = Pencatatan::orderBy('pencatatans.updated_at', 'desc')
                    ->whereMonth('pencatatans.created_at', $_temp[1])
                    ->whereYear('pencatatans.created_at', $_temp[0])
                    ->get();
                break;
            case 'tahunan':
                //Query Filter Tahun
                $datas = Pencatatan::orderBy('pencatatans.updated_at', 'desc')
                    ->whereYear('pencatatans.created_at', '=', $request->pilihan)
                    ->get();
                break;
            default:
                $datas = Pencatatan::all();
        }
        $filter = $request->filter;
        $pilihan = $request->pilihan;
        return view('export.menu.buku_besar',compact('no','datas','filter','pilihan'));
    }

    /**
     * Report Pengeluaran
     */
    public function pengeluaran(Request $request)
    {
        $t = now(); 

        $t = explode(" ", $t);
        $t = explode("-", $t[0]);
        $tanggal = "{$t[2]} {$t[1]} {$t[0]}";
        $no=1;

        $user= Auth::user()->nama;
        $rincian = "Pengeluaran";
        $title = "Laporan Pengeluaran";
        switch ($request->filter) {
            case 'harian':
                //Query Filter Harian
                $datas = Expense::orderBy('expenses.updated_at', 'desc')
                    ->whereDate('expenses.created_at', $request->pilihan)
                    ->get();
                break;

            case 'bulanan':
                //Query Filter Bulanan
                $temp = $request->pilihan;
                $_temp = explode('-', $temp);
                $datas = Expense::orderBy('expenses.updated_at', 'desc')
                    ->whereMonth('expenses.created_at', $_temp[1])
                    ->whereYear('expenses.created_at', $_temp[0])
                    ->get();
                break;

            case 'tahunan':
                //Query Filter Tahun
                $datas = Expense::orderBy('expenses.updated_at', 'desc')
                    ->whereYear('expenses.created_at', '=', $request->pilihan)
                    ->get();
                break;
            default:
                $datas = Expense::orderBy('expenses.updated_at', 'desc')
                    ->get();
        }
        $pdf = PDF::loadView('export.pengeluaran',compact('tanggal','user','rincian','datas','no','title'));
        $pdf->setPaper('A4', 'potrait');
        return $pdf->stream();
    }

    /**
     * Report Pemasukan
     */
    public function pemasukan(Request $request)
    {
        $t = now(); 
        $t = explode(" ", $t);
        $t = explode("-", $t[0]);
        $tanggal = "{$t[2]} {$t[1]} {$t[0]}";
        $start = date($this->parseDate($request->start));
        $end = date($this->parseDate($request->end));
        $no=1;
        $datas = Pencatatan::orderBy('pencatatans.updated_at', 'desc')
        ->join('incomes','incomes.id','=','pencatatans.income_id')
        ->whereBetween('pencatatans.created_at', [$start, $end])
        ->where([
            ['debit','<>','0'],
        ])->get();
        $user= Auth::user()->nama;
        $rincian = "Pemasukan";
        $title = "Laporan Pemasukan";
        $start = date($this->parseDateFormat($request->start));
        $end = date($this->parseDateFormat($request->end));
        return view('export.pemasukan', compact('tanggal','user','rincian','datas','no','title', 'start', 'end'));
    }

    /**
     * Report Buku Besar
     */
    public function bukuBesar(Request $request)
    {
        // ini_set('memory_limit','512MB');
        $t = now(); 

        $t = explode(" ", $t);
        $t = explode("-", $t[0]);
        $tanggal = "{$t[2]} {$t[1]} {$t[0]}";
        $no=1;

        // $datas = [];
        
        $query = Pencatatan::whereNotNull('id');

        if ($request->filter == "tahunan") {
            $query->whereYear("created_at", $request->pilihan);
        } else if ($request->filter == "bulanan") {
            $t = explode("-", $request->pilihan);
            $query->whereYear("created_at", $t[0]);
            $query->whereMonth("created_at", $t[1]);
        } else if ($request->filter == "harian") {
            $query->whereDate("created_at", $request->pilihan);
        }
        $datas = $query->get();

        $user= Auth::user()->nama;
        
        $rincian = "Buku Besar";
        $title = "Buku Besar";
        return view('export.bukubesar',compact('tanggal','user','rincian','datas','no','title'));
        // $pdf = PDF::loadView('export.bukubesar',compact('tanggal','user','rincian','datas','no','title'));
        // $pdf->setPaper('A4', 'potrait');
        // return $pdf->stream();
    }

    /**
     * Filtering untuk kategori pengeluaran
     * @return Expense
     */
    public function ajaxPengeluaran($stat)
    {
        switch ($stat) {
            case 'harian':
                //Filter Tanggal Pengeluaran
                return Expense::selectRaw('DATE_FORMAT(created_at, "%W, %d-%M-%Y") as tanggal, DATE(created_at) as tanggal_value')
                    ->groupBy('tanggal_value')
                    ->orderBy('created_at','DESC')
                    ->get();
                break;
            case 'bulanan':
                //Filter Bulan Pengeluaran
                return Expense::selectRaw('DATE_FORMAT(created_at, "%M-%Y") as tanggal, DATE_FORMAT(created_at, "%Y-%m") as tanggal_value')
                    ->groupBy('tanggal_value')
                    ->orderBy('created_at','DESC')
                    ->get();
                break;
            case 'tahunan':
                //Filter Tahun Pengeluaran
                return Expense::selectRaw('DATE_FORMAT(created_at, "%Y") as tanggal, DATE_FORMAT(created_at, "%Y") as tanggal_value')
                    ->groupBy('tanggal_value')
                    ->orderBy('created_at','DESC')
                    ->get();
                break;
        }   
    }

    /**
     * Filtering untuk kategori pemasukan
     * @return Income
     */
    public function ajaxPemasukan($stat)
    {
        switch ($stat) {
            case 'harian':
                //Filter Tanggal Pengeluaran
                return Income::selectRaw('DATE_FORMAT(created_at, "%W, %d-%M-%Y") as tanggal, DATE(created_at) as tanggal_value')
                    ->groupBy('tanggal_value')
                    ->orderBy('created_at','DESC')
                    ->get();
                break;
            case 'bulanan':
                //Filter Bulan Pengeluaran
                return Income::selectRaw('DATE_FORMAT(created_at, "%M-%Y") as tanggal, DATE_FORMAT(created_at, "%Y-%m") as tanggal_value')
                    ->groupBy('tanggal_value')
                    ->orderBy('created_at','DESC')
                    ->get();
                break;
            case 'tahunan':
                //Filter Tahun Pengeluaran
                return Income::selectRaw('DATE_FORMAT(created_at, "%Y") as tanggal, DATE_FORMAT(created_at, "%Y") as tanggal_value')
                    ->groupBy('tanggal_value')
                    ->orderBy('created_at','DESC')
                    ->get();
                break;
        }   
    }

    /**
     * Filtering untuk kategori Buku Besar
     * @return Pencatatan
     */
    public function ajaxBukuBesar($stat)
    {
        switch ($stat) {
            case 'harian':
                //Filter Tanggal Pengeluaran
                return Pencatatan::selectRaw('DATE_FORMAT(created_at, "%W, %d-%M-%Y") as tanggal, DATE(created_at) as tanggal_value')
                    ->groupBy('tanggal_value')
                    ->orderBy('created_at','DESC')
                    ->get();
                break;
            case 'bulanan':
                //Filter Bulan Pengeluaran
                return Pencatatan::selectRaw('DATE_FORMAT(created_at, "%M-%Y") as tanggal, DATE_FORMAT(created_at, "%Y-%m") as tanggal_value')
                    ->groupBy('tanggal_value')
                    ->orderBy('created_at','DESC')
                    ->get();
                break;
            case 'tahunan':
                //Filter Tahun Pengeluaran
                return Pencatatan::selectRaw('DATE_FORMAT(created_at, "%Y") as tanggal, DATE_FORMAT(created_at, "%Y") as tanggal_value')
                    ->groupBy('tanggal_value')
                    ->orderBy('created_at','DESC')
                    ->get();
                break;
        }   
    }
}
