<?php

namespace App\Http\Controllers;

use App\Payment;
use App\Student;
use App\PaymentDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PerhitunganTunggakanSiswaController extends Controller
{
    public function siswa($id = false)
    {
        $res = array(
            'besaran' => 0,
            'besaran_format' => 0,
            'potongan' => 0,
            'potongan_format' => 0,
            'terbayar' => 0,
            'terbayar_format' => 0,
            'sisa' => 0,
            'sisa_format' => 0,
        );
        if ($id) {
            $datas = DB::table('payment_details')
                    ->select(DB::raw('
                    financing_categories.id as financing_category_id, 
                    financing_categories.nama, 
                    financing_categories.jenis, 
                    count(payment_details.status) as banyak_tunggakan,
                    financing_periodes.nominal,
                    financing_periodes.kelas_x,
                    financing_periodes.kelas_xi,
                    financing_periodes.kelas_xii,
                    getNominalCicilan(payment_details.id) as cicilan_dibayar,
                    payment_details.payment_id,
                    payments.jenis_pembayaran,
                    payments.jenis_potongan,
                    payments.nominal_potongan,
                    payments.persentase'))
                    ->join('payments', 'payments.id', '=', 'payment_details.payment_id')
                    ->join('financing_periodes', 'financing_periodes.id', '=', 'payment_details.payment_periode_id')
                    ->join('financing_categories', 'financing_categories.id', '=', 'payments.financing_category_id')
                    ->orderBy('financing_categories.id')
                    ->groupBy('payment_details.payment_id')
                    ->where('payment_details.status','<>','Lunas')
                    ->where('payments.student_id','=', $id)
                    ->get();
            $total = [0,0,0,0];
            foreach ($datas as $i => $data) {
                $besaran_ = intval($data->nominal);
                $terbayar_ = $data->cicilan_dibayar == null ? 0 : intval($data->cicilan_dibayar);
                if ($data->jenis != "Sekali Bayar")
                {
                    $kelas_x = $data->kelas_x;
                    $kelas_xi = $data->kelas_xi;
                    $kelas_xii = $data->kelas_xii;
                    $besaran_ = $kelas_x*12 + $kelas_xi*12 + $kelas_xii*12;
                    $bt = $this->hitungBulanBayarSPP($data->payment_id);
                    $terbayar_ = intval(($bt[0] * $kelas_x) + ($bt[1] * $kelas_xi) + ($bt[2] * $kelas_xii));
                    $potongan_ = 0;
                } else {   
                    $potongan_ = intval($data->persentase) * intval($data->nominal) / 100;
                    if ($data->jenis_potongan === "nominal") {
                        $potongan_ = intval($data->nominal_potongan);
                    }
                }
    
                $sisa_ = $besaran_ - ( $terbayar_ + $potongan_ );
                
                $total[0] += $besaran_;
                $total[1] += $potongan_;
                $total[2] += $terbayar_;
                $total[3] += $sisa_;
            }
            $res = array(
                'besaran' => $total[0],
                'besaran_format' => number_format($total[0],0,',','.'),
                'potongan' => $total[1],
                'potongan_format' => number_format($total[1],0,',','.'),
                'terbayar' => $total[2],
                'terbayar_format' => number_format($total[2],0,',','.'),
                'sisa' => $total[3],
                'sisa_format' => number_format($total[3],0,',','.'),
            );
        }
        return $res;
    }

    public function hitungBulanBayarSPP($id)
    {
        $res = array(
            0,
            0,
            0,
        );
        if ($id) {
            $data = PaymentDetail::where('payment_id', $id)->get();
            $ir = -1;
            foreach ($data as $key => $value) {
                if ($key%12 == 0 && $ir < 3) {
                    $ir++;
                }
                if ($value->status == "Lunas") {
                    $res[$ir]++;
                }
            }
        }
        return $res;
    }

    public function process($siswa)
    {
        $summary = [0,0,0,0];
        $row = [];
        foreach ($siswa as $key => $value) {
            
            $angkatan = $value->angkatans->angkatan . " (" . $value->angkatans->tahun . ")";
            
            $temp = explode("_", $value->uname);
            $id = $temp[count($temp)-1];

            $item = array (
                "id" => $id,
                "nama" => $value->nama,
                "kelas" => $value->kelas,
                "jurusan" => $value->major->inisial,
                "angkatan" => $angkatan,
            );
            $data_tunggakan = $this->siswa($id);
            $row[] = array_merge($item, $data_tunggakan);
            $summary[0] += $data_tunggakan['besaran'];
            $summary[1] += $data_tunggakan['potongan'];
            $summary[2] += $data_tunggakan['terbayar'];
            $summary[3] += $data_tunggakan['sisa'];
        }
        return [
            'row' => $row,
            'summary' => $summary,
        ];
    }
}
