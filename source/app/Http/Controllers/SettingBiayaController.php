<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SettingBiaya;
use App\Major;
use App\Student;
use App\Angkatan;
use Illuminate\Support\Facades\Session;

use DB;

class SettingBiayaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no=1;
        $fil = '';
        $fil2 = '';
        $kls = '';
        $majors = Major::all();
        $angkatan = Angkatan::all();
        $students = DB::table('setting_biaya')
        ->select('students.*','setting_biaya.biaya','majors.nama as nama','angkatans.angkatan as angkatan','angkatans.tahun as tahun','students.nama as nama_siswa')
        ->join('students','students.id','setting_biaya.id_student')
        ->join('majors','majors.id','students.major_id')
        ->join('angkatans','angkatans.id','students.angkatan_id')
        ->where('angkatans.tahun','2018')
        ->get();

        $jml = $majors->count();

        return view('settingBiaya.setting_biaya', compact('students','no','jml','majors','fil','kls','angkatan','fil2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function updates(Request $request, $id)
    {
        // echo $id;die;
        try {
            $req = $request->all();
            DB::table('setting_biaya')->where('id_student',$id)->update([
                'biaya' => $req['biaya']
            ]);
            return redirect()
              ->route('setting_biaya.index')
              ->with('success', 'Data biaya berhasil diubah!');

        } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e){
          return redirect()
              ->route('setting_biaya.index')
              ->with('error', 'Data biaya gagal diubah!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
