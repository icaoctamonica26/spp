<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingBiaya extends Model
{
    /**
     * Nama table yang digunakan
     */
    protected $table = 'setting_biaya';

    /**
     * Kolom yang dapat di isi
     */
    protected $fillable = [
        "id",
        "id_student",
        "biaya",
        "created_at"
    ];
}
