<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Student;
use Illuminate\Support\Str;

class AddUnameKeyToStudent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->string('uname');
            $table->string('key');
        });

        $students = Student::all();
        foreach ($students as $i => $value) {
            $temp = explode(" ", $value['nama']);
            $uname = join("_",$temp);
            $uname .= "_" . $value['id'];
            $uname = strtolower($uname);
            $key = Str::random(6);
            $students[$i]['uname'] = $uname;
            $students[$i]['key'] = $key;
            $students[$i]->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropColumn(['uname', 'key']);
        });
    }
}
