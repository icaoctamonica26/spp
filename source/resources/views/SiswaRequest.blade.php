@extends('layouts.login')

@section('title')
Cek Tunggakan Siswa
@endsection

@section('content')
<h3 style="text-align:center">Cek Tunggakan Siswa</h3>
<hr>
<form method="get" id="loginForm">
    @csrf

    <div class="form-group ">
        <label for="uname" class="control-label">Uname</label>
        <input id="uname" type="text" class="form-control" name="uname" title="Please enter you uname" required autofocus>
    </div>

    <div class="form-group">
        <label for="key" class="control-label">Key</label>
        <input id="key" type="password" class="form-control" name="key" required autocomplete="current-key">
    </div>

    <button class="btn btn-success btn-block loginbtn" type="submit">
        Submit
    </button>
</form>
@endsection
