@extends('layouts.app')

@section('title')
SPP | Backup Pembayaran
@endsection

@section('content')
<!-- Static Table Start -->
<div class="data-table-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <div class="container-sm">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div style="float:right;display:flex;flex-direction:column">

                                            <form action="{{route('pdf.print')}}" role="form" method="post"
                                                target="_blank">
                                                @csrf
                                                <input type="hidden" name="id" value="pemasukan">
                                                <input type="hidden" name="bulan" value="{{$report['bulan']}}">
                                                <input type="hidden" name="tahun" value="{{$report['tahun']}}">
                                                <!-- <button type="submit" style="color:white; margin-top:0"
                                                    class=" btn btn-info">
                                                    <i class="fa fa-print"></i>&nbsp; Cetak
                                                </button> -->
                                                
                                            </form>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <div id="toolbar">
                            </div>
                            <table id="table" data-toggle="table" data-pagination="true" data-search="true"
                                data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true"
                                data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true"
                                data-toolbar="#toolbar">
                                <thead>
                                    <tr>
                                        <th data-field="id">No</th>
                                        <th data-field="siswa">Siswa </th>
                                        <th data-field="action">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($datas as $data)
                                    @php

                                        $temp = strtotime($data->created_at);   
                                        $tanggal2 = date('d/m/Y', $temp);
                                        $tanggal = date('j - M - Y', $temp);
                                    @endphp
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$data->nama}}</td>
                                        <td>
                                            <a href="{{ route('backpembayaran.detail',$data->id_student) }}" class="btn btn-success" title="Detail Data"><i class="fa fa-eye"></i></a>

                                            <a href="{{ route('backpembayaran.detail',$data->id_student) }}" class="btn btn-danger"
                                                onclick="event.preventDefault();destroy('{{ route('backpembayaran.destroy',$data->id_student) }}');"
                                                title="Hapus Data"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Static Table End -->
    <!-- hapus -->
    <form id="destroy-form" method="POST">
        @method('DELETE')
        @csrf
    </form>
    @endsection

    @push('styles')
    <!-- x-editor CSS  -->
    <link rel="stylesheet" href="{{ asset('assets/css/editor/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/editor/datetimepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/editor/bootstrap-editable.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/editor/x-editor-style.css') }}">
    <!-- normalize CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/data-table/bootstrap-table.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/data-table/bootstrap-editable.css') }}">

    <!-- forms CSS
        ============================================ -->
    <link rel="stylesheet" href="{{asset('assets/css/form/all-type-forms.css')}}">
    <!-- chosen CSS
        ============================================ -->
    <link rel="stylesheet" href="{{asset('assets/css/chosen/bootstrap-chosen.css')}}">
    <!-- datapicker CSS
        ============================================ -->
    <link rel="stylesheet" href="{{asset('assets/css/datapicker/datepicker3.css')}}">
    @endpush

    @push('scripts')

    <script>
        function editConfirm(tanggal, id, title, description, sumber, nominal) {
            $('#title').attr('value', title);
            $('#description').html(description);
            $('#nominal').attr('value', nominal);
            $('#tanggal_edit').attr('value', tanggal);

            $('#sumber_edit').val(sumber);
            $('#sumber_edit_chosen .chosen-single span').html(sumber);

            $('#editPemasukan').attr('action', "{{ url('income') }}/" + id)
            $('#modalUpdate').modal();
        }

        function cetakConfirm() {
            $('#modalCetak').modal();
        }

        function convertDate(date) {
            let temp = date.split(" ");
            let temp_date = temp[0];
            let oke = temp_date.split("-");
            let fix = oke[1] + "/" + oke[2] + "/" + oke[0];
            return fix;
        }

        function destroy(action) {
            swal({
                title: 'Apakah anda yakin?',
                text: 'Setelah dihapus, Anda tidak akan dapat mengembalikan data ini!',
                icon: 'warning',
                buttons: ["Cancel", "Yes!"],
            }).then(function (value) {
                if (value) {
                    document.getElementById('destroy-form').setAttribute('action', action);
                    document.getElementById('destroy-form').submit();
                } else {
                    swal("Data kamu aman!");
                }
            });
        }

    </script>

    @endpush

    @push('scripts-asset')
    <!-- data table JS
        ============================================ -->
    <script src="{{ asset('assets/js/data-table/bootstrap-table.js') }}"></script>
    <script src="{{ asset('assets/js/data-table/tableExport.js') }}"></script>
    <script src="{{ asset('assets/js/data-table/data-table-active.js') }}"></script>
    <script src="{{ asset('assets/js/data-table/bootstrap-table-editable.js') }}"></script>
    <script src="{{ asset('assets/js/data-table/bootstrap-editable.js') }}"></script>
    <script src="{{ asset('assets/js/data-table/bootstrap-table-resizable.js') }}"></script>
    <script src="{{ asset('assets/js/data-table/colResizable-1.5.source.js') }}"></script>
    <script src="{{ asset('assets/js/data-table/bootstrap-table-export.js') }}"></script>
    <!--  editable JS
        ============================================ -->
    <script src="{{ asset('assets/js/editable/jquery.mockjax.js') }}"></script>
    <script src="{{ asset('assets/js/editable/mock-active.js') }}"></script>
    <script src="{{ asset('assets/js/editable/select2.js') }}"></script>
    <script src="{{ asset('assets/js/editable/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/editable/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/js/editable/bootstrap-editable.js') }}"></script>
    <script src="{{ asset('assets/js/editable/xediable-active.js') }}"></script>

    <!-- icheck JS
        ============================================ -->
    <script src="{{ asset('assets/js/icheck/icheck.min.js')}}"></script>
    <script src="{{ asset('assets/js/icheck/icheck-active.js')}}"></script>

    <!-- chosen JS
        ============================================ -->
    <script src="{{ asset('assets/js/chosen/chosen.jquery.js')}}"></script>
    <script src="{{ asset('assets/js/chosen/chosen-active.js')}}"></script>

    <!-- input-mask JS
        ============================================ -->
    <script src="{{ asset('assets/js/input-mask/jasny-bootstrap.min.js')}}"></script>

    <!-- datapicker JS
        ============================================ -->
    <script src="{{ asset('assets/js/datapicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{ asset('assets/js/datapicker/datepicker-active.js')}}"></script>
    @endpush

    @push('breadcrumb-left')
    <h3>Data Backup Pembayaran</h3>
    @endpush

    @push('breadcrumb-right')
    <div style="float:right">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="margin-bottom:0">
                <li class="breadcrumb-item"><a href="{{ url('/')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Pembayaran</li>
            </ol>
        </nav>
    </div>
    @endpush
