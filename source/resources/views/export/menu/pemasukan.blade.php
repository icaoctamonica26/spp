@extends('layouts.app')

@section('title')
SPP | Laporan Pemasukan
@endsection

@section('content')
<!-- Static Table Start -->
<div class="data-table-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <div class="container-sm">
                                <div class="row">
                                    <div class="col-md-6">
                                        {{-- <form action="{{route('rekap.pemasukan.filter')}}" role="form" method="post">
                                        @csrf
                                            <div style="float:left; display:flex; flex-direction:row; max-height:55">
                                                <select class="form-control" style="margin-right:5px; width:150px" name="filter" id="filter" required>
                                                    <option value="all">Semua</option>
                                                    <option value="tahunan">Tahunan </option>
                                                    <option value="bulanan">Bulanan </option>
                                                    <option value="harian">Harian </option>
                                                </select>
                                                <select class="form-control" name="pilihan" id="pilihan" required>
                                                    <option value="">-- Pilih --</option>
                                                </select>
                                                <button type='submit' class="btn btn-info" style="margin-left:5px;">Filter</button>
                                            </div>
                                        </form> --}}
                                    </div>
                                    <div class="col-md-6">
                                        <a class="btn btn-primary pull-right" href="#" data-toggle="modal" data-target="#PrimaryModalhdbgcl"><i class="fa fa-cog" ></i> Option</a>
                                        <a class="btn btn-danger pull-right" href="#" style="margin-right : 10px" id="reset"><i class="fa fa-circle" ></i> Reset</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <div id="toolbar">
                            </div>
                            <table id="table" 
                            data-toggle="table" 
                            data-pagination="true" 
                            data-search="false" 
                            data-show-columns="true" 
                            data-show-pagination-switch="true" 
                            data-show-refresh="true" 
                            data-key-events="true" 
                            data-show-toggle="true" 
                            data-resizable="true" 
                            data-cookie="true"
                            data-cookie-id-table="saveId" 
                            data-show-export="true" 
                            data-show-footer="true"
                            data-query-params="queryParams"
                            data-footer-style="footerStyle"
                            data-click-to-select="true"
                            data-side-pagination="server"
                            data-url="{{ url('') }}/rekap_Pemasukan/data_master"
                            data-toolbar="#toolbar">
                                <thead>
                                    <tr>
                                        <th data-field="id" data-visible="false" data-halign="center" data-align="center">ID</th>
                                        <th data-field="no" data-width="20" data-halign="center" data-align="center">No</th>
                                        <th data-field="foto" data-width="100" data-halign="center" data-align="center">Foto</th>
                                        <th data-field="tanggal" data-width="50" data-halign="center" data-align="center">Tanggal</th>
                                        <th data-field="description" data-halign="center" data-align="justify" >Deskripsi</th>
                                        <th data-field="nominal" data-width="100" data-halign="center" data-align="right" data-footer-formatter="totalFormatter"
										data-formatter="parseRupiah">Nominal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    </tbody>
                                    <tfoot></tfoot>
                                    </table>
                                    <div class="container-sm" style="margin-top:10px">
                                        <div class="row">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div style="float:right; margin-right:20%">
                                                    <div class="row">
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Total </td>
                                                                    <td>:</td>
                                                                    <td>Rp.</td>
                                                                    <td>
                                                                        <div style="text-align: right">
                                                                            <span class="" style="font-size:24px;color:green">
                                                                                <strong id="total"></strong>
                                                                            </span>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Static Table End -->
        <div id="PrimaryModalhdbgcl" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-color-modal bg-color-1">
                        <h4 class="modal-title">Filter Laporan</h4>
                        <div class="modal-close-area modal-close-df">
                            <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group data-custon-pick data-custom-mg" id="data_5">
                            <form action="{{route('rekap.pemasukan.export')}}" target="_blank" role="form" id="cetak" method="post">
                                @csrf
                                <div class="form-group" style="width: 100%">
                                    <label for="" style="font-weight: bold">Periode Laporan</label> 
                                    <input type="hidden" name="date_status" value="0">
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input type="text" class="form-control" name="start" value="{{date('m/d/Y')}}"/>
                                        <span class="input-group-addon">sampai</span>
                                        <input type="text" class="form-control" name="end" value="{{date('m/d/Y')}}"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a data-dismiss="modal" href="#" style="background-color: red">Cancel</a>
                        <a href="#" style="background-color: green" id="filterTombolModal">Filter</a>
                        <a href="#" id="cetakTombolModal">Cetak</a>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @push('styles')
        <!-- x-editor CSS  -->
        <link rel="stylesheet" href="{{ asset('assets/css/editor/select2.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/editor/datetimepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/editor/bootstrap-editable.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/editor/x-editor-style.css') }}">
        <!-- normalize CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/data-table/bootstrap-table.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/data-table/bootstrap-editable.css') }}">

        <!-- forms CSS
		============================================ -->
        <link rel="stylesheet" href="{{asset('assets/css/form/all-type-forms.css')}}">
        <!-- chosen CSS
		============================================ -->
        <link rel="stylesheet" href="{{asset('assets/css/chosen/bootstrap-chosen.css')}}">

        <!-- datapicker CSS
		============================================ -->
        <link rel="stylesheet" href="{{asset('assets/css/datapicker/datepicker3.css')}}">

        <style>
            kode {
                color: red;
            }

        </style>
        @endpush

        @push('scripts')

        @endpush

        @push('scripts-asset')
        <!-- data table JS
		============================================ -->
        <script src="{{ asset('assets/js/data-table/bootstrap-table.js') }}"></script>
        <script src="{{ asset('assets/js/data-table/tableExport.js') }}"></script>
        <script src="{{ asset('assets/js/data-table/data-table-active.js') }}"></script>
        <script src="{{ asset('assets/js/data-table/bootstrap-table-editable.js') }}"></script>
        <script src="{{ asset('assets/js/data-table/bootstrap-editable.js') }}"></script>
        <script src="{{ asset('assets/js/data-table/bootstrap-table-resizable.js') }}"></script>
        <script src="{{ asset('assets/js/data-table/colResizable-1.5.source.js') }}"></script>
        <script src="{{ asset('assets/js/data-table/bootstrap-table-export.js') }}"></script>
        <!--  editable JS
		============================================ -->
        <script src="{{ asset('assets/js/editable/jquery.mockjax.js') }}"></script>
        <script src="{{ asset('assets/js/editable/mock-active.js') }}"></script>
        <script src="{{ asset('assets/js/editable/select2.js') }}"></script>
        <script src="{{ asset('assets/js/editable/moment.min.js') }}"></script>
        <script src="{{ asset('assets/js/editable/bootstrap-datetimepicker.js') }}"></script>
        <script src="{{ asset('assets/js/editable/bootstrap-editable.js') }}"></script>
        <script src="{{ asset('assets/js/editable/xediable-active.js') }}"></script>

        <!-- icheck JS
		============================================ -->
        <script src="{{ asset('assets/js/icheck/icheck.min.js')}}"></script>
        <script src="{{ asset('assets/js/icheck/icheck-active.js')}}"></script>

        <!-- chosen JS
		============================================ -->
        <script src="{{ asset('assets/js/chosen/chosen.jquery.js')}}"></script>
        <script src="{{ asset('assets/js/chosen/chosen-active.js')}}"></script>

        <!-- input-mask JS
		============================================ -->
        <script src="{{ asset('assets/js/input-mask/jasny-bootstrap.min.js')}}"></script>

        <!-- datapicker JS
		============================================ -->
        <script src="{{ asset('assets/js/datapicker/bootstrap-datepicker.js')}}"></script>
        <script src="{{ asset('assets/js/datapicker/datepicker-active.js')}}"></script>

        <!-- Custom Script -->
        <!-- ============================================ -->
        <script>
            function change_filter() {
                var filter = $('#filter').val();
                if(filter === 'all')
                {
                    var cek = $('input[type=hidden][name=filter]').val();
                    if(cek === '')
                    {
                        $('input[type=hidden][name=filter]').val(filter);
                        $('input[type=hidden][name=pilihan]').val(filter);
                    }
                    $('#pilihan').empty();
                    $('#pilihan').append(`<option value="all">Semua</option>`);
                }
                else
                {
                    $('input[type=hidden][name=pilihan]').val('');
                    $('input[type=hidden][name=filter]').val(filter);
                    $.get(`{{ url('') }}/rekap_Pemasukan/ajax/${filter}`, function(data){
                        var temp = {tanggal: "-- Pilih --", tanggal_value: ""};
                        data.unshift(temp);
                        $('#pilihan').empty();
                        $.each(data, function (i, val){
                            $('#pilihan').append(`<option value="${val.tanggal_value}">${val.tanggal}</option>`);
                        });
                    });
                }
            }
            function change_pilihan() {
                var pilihan = $('#pilihan').val();
                var filter = $('#filter').val();
                if(filter != 'all')
                {
                    $('input[type=hidden][name=pilihan]').val(pilihan);
                }
            }
            function validate() {
                var pilihan = $('input[type=hidden][name=pilihan]').val();
                if (pilihan === '')
                {
                    swal({
                        title: 'Peringatan',
                        text: 'Form pilihan filter tidak boleh kosong!',
                        icon: 'warning',
                    });
                }
                else
                {
                    $('#cetak').submit();
                }
            }
            $('#filter').change(change_filter);
            $('#pilihan').change(change_pilihan);
            $(document).ready(function(){
                $.get("{{ url('')}}/rekap_Pemasukan/total", function(res){
                    $('#total').html(parseRupiah(res));
                });
                $('#filterTombolModal').click(function(){
                    $('input[type=hidden][name=date_status]').val(1)
                    $('#PrimaryModalhdbgcl').modal('toggle');
                    $('#table').bootstrapTable('refresh');
                });
                $('#reset').click(function(){
                    $('input[type=hidden][name=date_status]').val(0)
                    $('#table').bootstrapTable('refresh');
                });
                $('#cetakTombolModal').click(function(){
                    $('#cetak').submit();
                });
                change_filter();
                change_pilihan();
            });
            function parseRupiah (bilangan)
            {
                var	reverse = bilangan.toString().split('').reverse().join(''),
                    ribuan 	= reverse.match(/\d{1,3}/g);
                    ribuan	= ribuan.join('.').split('').reverse().join('');
                return ribuan;
            }
            function totalFormatter(data) {
                var field = this.field
                let d = data[0];
                if (d != undefined) {
                    return '<b>' + 'Rp. ' + parseRupiah(data.map(function (row) {
                        return +row[field]
                    }).reduce(function (sum, i) {
                        return sum + i
                    }, 0)) + '&nbsp;&nbsp;</b>'
                }
            }
            function queryParams(params) {
                params.date_start = $('input[name=start]').val()
                params.date_end = $('input[name=end]').val()
                params.is_date = $('input[type=hidden][name=date_status]').val()
                return params
            }
        </script>
        @endpush

        @push('breadcrumb-left')
        <div class="col-md-1" style="item-align:center">
            <a href="{{ url('/rekap')}}" class="btn btn-primary" href="#" title="Kembali"><i class="fa fa-arrow-left" ></i></a>
        </div>
        <div class="col-md-11">
            <div style="margin-left:15px;">
                <h3>Data Laporan Pemasukan</h3>
            </div>
        </div>
        @endpush


        @push('breadcrumb-right')
        <div style="float:right">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb" style="margin-bottom:0">
                    <li class="breadcrumb-item"><a href="{{ url('/')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('/rekap')}}">Rekap</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Laporan Pemasukan</li>
                </ol>
            </nav>
        </div>
        @endpush
