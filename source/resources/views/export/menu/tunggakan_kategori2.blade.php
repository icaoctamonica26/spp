@extends('layouts.app')

@section('title')
SPP | Laporan Tunggakan
@endsection

@section('content')
<!-- Static Table Start -->
<div class="data-table-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <div class="container-sm">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div style="float:left; display:flex; flex-direction:row; max-height:55">
                                            <select class="form-control" style="margin-right:5px; width:150px"
                                                name="filter" id="filter" required>
                                            </select>
                                            <select class="form-control" style="margin-right:5px; width:150px"
                                                name="kelas" id="kelas" required>
                                            </select>
                                            <select class="form-control" style="margin-right:5px; width:150px"
                                                name="jurusan" id="jurusan" required>
                                            </select>
                                            <button onclick="filterData()" class="btn btn-info"
                                                style="margin-left:5px;">Filter</button>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        {{-- <a class="btn btn-primary pull-right" href="#" data-toggle="modal" data-target="#PrimaryModalhdbgcl"><i class="fa fa-print" ></i> Cetak</a> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sparkline13-graph">
                    <div class="datatable-dashv1-list custom-datatable-overright">
                        <style>
                            #toolbar {
                                margin: 0;
                            }

                        </style>
                        <div id="toolbar" class="select">
                            <select class="form-control">
                                <option value="">Export Standar</option>
                                <option value="selected">Export Terpilih</option>
                            </select>
                        </div>
                        <table id="table"></table>
                        {{-- <table id="table" 
                            data-toggle="table"
                            data-export-option='{"fileName": "Rekapitulasi Tunggakan Keuangan Siswa"}'
                            data-search="true" 
                            data-show-columns="true"
                            data-show-refresh="true" 
                            data-key-events="true" 
                            data-show-toggle="true" 
                            data-resizable="true"
                            data-export-types="['excel', 'xlsx']"
                            data-page-list="[10, 25, 50, 100, 200, 500, 1000, 2000, 3000, 5000, 10000]"
                            data-pagination="true" 
                            data-show-pagination-switch="true" 
                            data-side-pagination="server"
                            data-url="{{ url('') }}/rekap_tunggakan/data_master/Kategori"
                        data-show-footer="true"
                        data-footer-style="footerStyle"
                        data-cookie="true"
                        data-query-params="queryParams"
                        data-cookie-id-table="saveId"
                        data-show-export="true"
                        data-checkbox-header="false"
                        data-click-to-select="true"
                        data-toolbar="#toolbar" >
                        <thead>
                            <tr>
                                <th data-checkbox="true" data-width="20"></th>
                                <th data-field="id" data-visible="false">Id</th>
                                <th data-formatter="runningFormatter" data-width="30" data-halign="center"
                                    data-align="center">No</th>
                                <th data-field="kategori" data-width="100" data-halign="center" data-align="center"
                                    data-sortable="true">Kategori</th>
                                <th data-field="nama" data-sortable="true" data-halign="center">Nama</th>
                                <th data-field="kelas" data-width="100" data-halign="center" data-align="center">Kelas
                                </th>
                                <th data-field="jurusan" data-width="100" data-halign="center" data-align="center">
                                    Jurusan</th>
                                <th data-field="angkatan" data-width="100" data-halign="center" data-align="center">
                                    Angkatan</th>
                                <th data-field="besaran" data-halign="center" data-width="150" data-align="right"
                                    data-footer-formatter="totalFormatter">Besaran</th>
                                <th data-field="potongan" data-halign="center" data-width="150" data-align="right"
                                    data-footer-formatter="totalFormatter">Potongan</th>
                                <th data-field="terbayar" data-halign="center" data-width="150" data-align="right"
                                    data-footer-formatter="totalFormatter">Terbayar</th>
                                <th data-field="sisa" data-halign="center" data-width="150" data-align="right"
                                    data-footer-formatter="totalFormatter">Sisa</th>
                                <th data-field="total" data-halign="center" data-width="150" data-align="right"
                                    data-footer-formatter="totalFormatter">Total</th>
                            </tr>
                        </thead>
                        <tfoot></tfoot>
                        </table> --}}
                        
                        <div class="container-sm" style="margin-top:10px">
                            <div class="row">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                    <div style="float:right; margin-right:20%">
                                        <div class="row">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td>Besaran Keseluruhan </td>
                                                        <td>:</td>
                                                        <td>Rp.</td>
                                                        <td>
                                                            <div style="text-align: right">
                                                                <span class="" style="font-size:20px;">
                                                                    <strong id="besaran_view_2">loading...</strong>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Potongan Keseluruhan </td>
                                                        <td>:</td>
                                                        <td>Rp.</td>
                                                        <td>
                                                            <div style="text-align: right">
                                                                <span class="" style="font-size:20px;">
                                                                    <strong id="potongan_view_2">loading...</strong>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Terbayar Keseluruhan </td>
                                                        <td>:</td>
                                                        <td>Rp.</td>
                                                        <td>
                                                            <div style="text-align: right">
                                                                <span class="" style="font-size:20px;">
                                                                    <strong id="terbayar_view_2">loading...</strong>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sisa Keseluruhan </td>
                                                        <td>:</td>
                                                        <td>Rp.</td>
                                                        <td>
                                                            <div style="text-align: right">
                                                                <span class="" style="font-size:24px;color:red">
                                                                    <strong id="sisa_view_2">loading...</strong>
                                                                </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div id="PrimaryModalhdbgcl" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header-color-modal bg-color-1">
                <h4 class="modal-title">Cetak Laporan</h4>
                <div class="modal-close-area modal-close-df">
                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                </div>
            </div>
            <div class="modal-body">
                <div class="form-group data-custon-pick data-custom-mg" id="data_5">
                    <form action="{{route('rekap.tunggakan.export')}}" target="_blank" role="form" id="cetak"
                        method="post">
                        @csrf
                        <input type="hidden" name="stat" value="{{$stat}}">
                        <input type="hidden" name="keyword" value="">
                        <input type="hidden" name="kelas" value="{{$kelas}}">
                        <input type="hidden" name="jurusan" value="{{$jurusan}}">
                        <input type="hidden" name="filter" value="{{$filter}}">
                        <div class="form-group" style="width: 100%">
                            <label for="" style="font-weight: bold">Pilih Kategori</label>
                            <div class="input-group date">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <select class="form-control" name="filter_select" id="mFilter" required>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" style="width: 100%">
                            <label for="" style="font-weight: bold">Pilih Angkatan</label>
                            <div class="input-group date">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <select class="form-control" name="kelas_select" id="mKelas" required>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" style="width: 100%">
                            <label for="" style="font-weight: bold">Pilih Jurusan</label>
                            <div class="input-group date">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <select class="form-control" name="jurusan_select" id="mJurusan" required>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" href="#" style="background-color: red">Cancel</a>
                <a href="#" id="cetakTombolModal">Process</a>
            </div>
        </div>
    </div>
</div>
<!-- Static Table End -->
@endsection

@push('styles')
<!-- x-editor CSS  -->
<link rel="stylesheet" href="{{ asset('assets/css/editor/select2.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/editor/datetimepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/editor/bootstrap-editable.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/editor/x-editor-style.css') }}">
<!-- normalize CSS -->
<link rel="stylesheet" href="{{ asset('assets/css/data-table/bootstrap-table.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/data-table/bootstrap-editable.css') }}">

<!-- forms CSS
		============================================ -->
<link rel="stylesheet" href="{{asset('assets/css/form/all-type-forms.css')}}">
<!-- chosen CSS
		============================================ -->
<link rel="stylesheet" href="{{asset('assets/css/chosen/bootstrap-chosen.css')}}">

<!-- datapicker CSS
		============================================ -->
<link rel="stylesheet" href="{{asset('assets/css/datapicker/datepicker3.css')}}">

<style>
    kode {
        color: red;
    }

</style>
@endpush

@push('scripts')

@endpush

@push('scripts-asset')
<!-- data table JS
		============================================ -->
<script src="{{ asset('assets/js/data-table/bootstrap-table.js') }}"></script>
<script src="{{ asset('assets/js/data-table/tableExport.js') }}"></script>
<script src="{{ asset('assets/js/data-table/data-table-active.js') }}"></script>
<script src="{{ asset('assets/js/data-table/bootstrap-table-editable.js') }}"></script>
<script src="{{ asset('assets/js/data-table/bootstrap-editable.js') }}"></script>
<script src="{{ asset('assets/js/data-table/bootstrap-table-resizable.js') }}"></script>
<script src="{{ asset('assets/js/data-table/colResizable-1.5.source.js') }}"></script>
<script src="{{ asset('assets/js/data-table/bootstrap-table-export.js') }}"></script>
<!--  editable JS
		============================================ -->
<script src="{{ asset('assets/js/editable/jquery.mockjax.js') }}"></script>
<script src="{{ asset('assets/js/editable/mock-active.js') }}"></script>
<script src="{{ asset('assets/js/editable/select2.js') }}"></script>
<script src="{{ asset('assets/js/editable/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/editable/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('assets/js/editable/bootstrap-editable.js') }}"></script>
<script src="{{ asset('assets/js/editable/xediable-active.js') }}"></script>

<!-- icheck JS
		============================================ -->
<script src="{{ asset('assets/js/icheck/icheck.min.js')}}"></script>
<script src="{{ asset('assets/js/icheck/icheck-active.js')}}"></script>

<!-- chosen JS
		============================================ -->
<script src="{{ asset('assets/js/chosen/chosen.jquery.js')}}"></script>
<script src="{{ asset('assets/js/chosen/chosen-active.js')}}"></script>

<!-- input-mask JS
		============================================ -->
<script src="{{ asset('assets/js/input-mask/jasny-bootstrap.min.js')}}"></script>

<!-- datapicker JS
		============================================ -->
<script src="{{ asset('assets/js/datapicker/bootstrap-datepicker.js')}}"></script>
<script src="{{ asset('assets/js/datapicker/datepicker-active.js')}}"></script>
<!-- Custom Script -->
<!-- ============================================ -->
<script>
    function queryParams(params) {
        params.kategori = $('select[name=filter]').children("option:selected").val();
        params.jurusan = $('select[name=jurusan]').children("option:selected").val();
        params.angkatan = $('select[name=kelas]').children("option:selected").val();
        $('input[type=hidden][name=filter]').val(params.filter);
        $('input[type=hidden][name=kelas]').val(params.angkatan);
        $('input[type=hidden][name=jurusan]').val(params.jurusan);
        return params
    }

    function idFormatter() {
        return '-'
    }

    function parseRupiah(bilangan) {
        var reverse = bilangan.toString().split('').reverse().join(''),
            ribuan = reverse.match(/\d{1,3}/g);
        ribuan = ribuan.join('.').split('').reverse().join('');
        return ribuan;
    }

    function totalFormatter(data) {
        var field = this.field
        let d = data[0];
        if (d != undefined) {
            return '<b>' + 'Rp. ' + parseRupiah(data.map(function (row) {
                return +row[field]
            }).reduce(function (sum, i) {
                return sum + i
            }, 0)) + '&nbsp;&nbsp;</b>'
        }
    }

    function runningFormatter(value, row, index) {
        $table = $('#table'); // your tablegrid id
        var tableOptions = $table.bootstrapTable('getOptions');
        return ((tableOptions.pageNumber - 1) * tableOptions.pageSize) + (1 + index);
    }

    function footerStyle(column) {
        return {
            css: {
                'width': '100%'
            },
        }
    }

    function change_filter() {
        var filter = $('#filter').val();
        var pilihan = $('input[type=hidden][name=filter]').val(filter);
    }

    function load_filter() {
        $.get(`{{ url('') }}/rekap_tunggakan/ajax/{{$stat}}/Kategori`, function (data) {
            var temp = {
                kategori: "Semua Kategori",
                kategori_value: "all"
            };
            data.unshift(temp);
            $('#filter').empty();
            $('#mFilter').empty();
            $.each(data, function (i, val) {
                $('#filter').append(`<option value="${val.kategori_value}">${val.kategori}</option>`);
                $('#mFilter').append(`<option value="${val.kategori_value}">${val.kategori}</option>`);
            });
        });
    }

    function change_kelas() {
        var kelas = $('#kelas').val();
        var pilihan = $('input[type=hidden][name=kelas]').val(kelas);
    }

    function change_jurusan() {
        var jurusan = $('#jurusan').val();
        var pilihan = $('input[type=hidden][name=jurusan]').val(jurusan);
    }

    function load_kelas() {
        $.get(`{{ url('') }}/angkatan/create`, function (data) {
            var temp = {
                kategori: "Semua Angkatan",
                kategori_value: "all"
            };
            data.unshift(temp);
            $('#kelas').empty();
            $('#mKelas').empty();
            $.each(data, function (i, val) {
                let v = val.id
                let s = `${val.angkatan} - (${val.tahun}) - ${val.status}`
                if (i === 0) {
                    v = val.kategori_value
                    s = val.kategori
                }
                $('#kelas').append(`<option value="${v}">${s}</option>`);
                $('#mKelas').append(`<option value="${v}">${s}</option>`);
            });
        });
    }

    function load_jurusan() {
        $.get(`{{ url('') }}/rekap_tunggakan/ajax/{{$stat}}/Jurusan`, function (data) {
            var temp = {
                kategori: "Semua Jurusan",
                kategori_value: "all"
            };
            data.unshift(temp);
            $('#jurusan').empty();
            $('#mJurusan').empty();
            $.each(data, function (i, val) {
                $('#jurusan').append(`<option value="${val.kategori_value}">${val.kategori}</option>`);
                $('#mJurusan').append(`<option value="${val.kategori_value}">${val.kategori}</option>`);
            });
        });
    }

    function validate() {
        // var filter = $('input[type=hidden][name=filter]').val();
        // if (filter === '')
        // {
        //     swal({
        //         title: 'Peringatan',
        //         text: 'Form pilihan filter tidak boleh kosong!',
        //         icon: 'warning',
        //     });
        // }
        // else
        // {
        //     $('#cetak').submit();
        // }
        var filter = $('#filter').val();
        var kelas = $('#kelas').val();
        var jurusan = $('#jurusan').val();

        $('input[type=hidden][name=filter]').val(filter);
        $('input[type=hidden][name=kelas]').val(kelas);
        $('input[type=hidden][name=jurusan]').val(jurusan);

        $('#cetak').submit();
    }
    $('#cetakTombolModal').click(function () {
        validate()
    });

    function hariIni() {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        return mm + '/' + dd + '/' + yyyy;
    }

    function change_search() {
        let val = $(this).val();
        var pilihan = $('input[type=hidden][name=keyword]').val(val);

        $.get(`{{ url('') }}/rekap_tunggakan/_ajax_kategori/data/${val}`, function (data) {
            data = JSON.parse(data);

            $('#besaran_view').text(data.besaran);
            $('#potongan_view').text(data.potongan);
            $('#terbayar_view').text(data.terbayar);
            $('#sisa_view').text(data.sisa);
        });
    }

    function filterData() {
        $('#table').bootstrapTable(
            'refresh'
        );
    }

    $('#filter').change(change_filter);
    $('#kelas').change(change_kelas);
    $('#jurusan').change(change_jurusan);
    $(document).ready(function () {
        load_filter();
        load_kelas();
        load_jurusan();
        // let search = document.getElementsByClassName('search');
        // let form = search[0].lastChild;
        // form.addEventListener("keyup", change_search);
        // $.get('{{ url('') }}/rekap/ajax/custom_tunggakan',
        //     function (data) {
        //         $('#besaran_view_2').html(parseRupiah(data[0]));
        //         $('#potongan_view_2').html(parseRupiah(data[1]));
        //         $('#terbayar_view_2').html(parseRupiah(data[2]));
        //         $('#sisa_view_2').html(parseRupiah(data[3]));
        //     }
        // )
    });

    var table = $('#table');
    var selections = [];

    function getIdSelections() {
        return $.map(table.bootstrapTable('getSelections'), function (row) {
            // console.log(' row select ');
            // console.log(row);
            // console.log('selections row');
            // console.log(selections);

            return row.id
        })
    }

    function parseRupiah(bilangan) {
        var number_string = bilangan.toString(),
            sisa = number_string.length % 3,
            rupiah = number_string.substr(0, sisa),
            ribuan = number_string.substr(sisa).match(/\d{3}/g);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        return rupiah;
    }

    $(function () {
        $('#table').bootstrapTable({
            search: true,
            resizable: true,
            exportOptions: {
                fileName: "Rekapitulasi Tunggakan Keuangan Siswa"
            },
            showColumns: true,
            showRefresh: true,
            keyEvents: true,
            showToggle: true,
            exportTypes: ['excel'],
            pageList: [10, 25, 50, 100, 300, 500, 1000, 2000, 3000, 5000, 10000],
            pagination: true,
            paginationSwitch: true,
            sidePagination: 'server',
            url: "{{ url('') }}/rekap_tunggakan/data_master/Kategori",
            showFooter: true,
            footerStyle: "footerStyle",
            cookie: true,
            queryParams: "queryParams",
            cookieIdTable: "saveId",
            showExport: true,
            clickToSelect: true,
            toolbar: "#toolbar",
            columns: [{
                checkbox: true,
                width: 20,
            }, {
                field: 'id',
                visible: false
            }, {
                title: "No",
                width: 30,
                halign: "center",
                align: "center",
                formatter: "runningFormatter"
            }, {
                field: "kategori",
                title: "Kategori",
                width: 100,
                halign: "center",
                align: "center",
                sortable: true,
            }, {
                field: "nama",
                title: "Nama",
                width: 100,
                halign: "center",
                align: "center",
                sortable: true,
            }, {
                field: "kelas",
                title: "Kelas",
                width: 100,
                halign: "center",
                align: "center",
            }, {
                field: "jurusan",
                title: "Jurusan",
                width: 100,
                halign: "center",
                align: "center",
            }, {
                field: "angkatan",
                title: "Angkatan",
                width: 100,
                halign: "center",
                align: "center",
            }, {
                field: "besaran",
                title: "Besaran",
                width: 150,
                halign: "center",
                align: "right",
                footerFormatter: "totalFormatter",
            }, {
                field: "potongan",
                title: "Potongan",
                width: 150,
                halign: "center",
                align: "right",
                footerFormatter: "totalFormatter",
            }, {
                field: "terbayar",
                title: "Terbayar",
                width: 150,
                halign: "center",
                align: "right",
                footerFormatter: "totalFormatter",
            }, {
                field: "sisa",
                title: "Sisa",
                width: 150,
                halign: "center",
                align: "right",
                footerFormatter: "totalFormatter",
            }, {
                field: "total",
                title: "Total",
                width: 150,
                halign: "center",
                align: "right",
                footerFormatter: "totalFormatter",
            }]
        })
    });
    $(function () {
        // let t = hariIni();
        // $('input[name=start]').val(t);
        // $('input[name=end]').val(t);
        // table.on(
        // 'search.bs.table page-change.bs.table',
        // function () {
        //     var temp = table.bootstrapTable('getData');
        //     var total = [0,0,0,0];
        //     var li = $('li .active').html();
        //     var opt = table.bootstrapTable('getOptions');
        //     var page_size = opt.pageSize;
        //     var page_total = opt.totalPages;
        //     var data_total = opt.totalRows;
        //     var page_number = opt.pageNumber;
        //     var limit = page_size * page_number;
        //     var i = limit - page_size;
        //     while (i < limit && i < data_total) {
        //         total[0] += temp[i]._besaran;
        //         total[1] += temp[i]._potongan;
        //         total[2] += temp[i]._terbayar;
        //         total[3] += temp[i]._sisa;
        //         i++;
        //     }
        //     $('#besaran_view').html(parseRupiah(total[0]));
        //     $('#potongan_view').html(parseRupiah(total[1]));
        //     $('#terbayar_view').html(parseRupiah(total[2]));
        //     $('#sisa_view').html(parseRupiah(total[3]));
        // })
        // $.get('{{ url('') }}/rekap_tunggakan/data_master/Kategori', function(data){
        //     setTable(data);
        // });
        // var $table = $('#table')
        // table.bootstrapTable({
        //     exportOptions: {
        //         fileName: 'Rekapitulasi Tunggakan Keuangan Siswa'
        //     }
        // });
        $('#toolbar').find('select').change(function () {
            $('#table').bootstrapTable('destroy').bootstrapTable({
                exportDataType: $(this).val(),
                search: true,
                resizable: true,
                exportOptions: {
                    fileName: "Rekapitulasi Tunggakan Keuangan Siswa"
                },
                showColumns: true,
                showRefresh: true,
                keyEvents: true,
                showToggle: true,
                exportTypes: ['excel'],
                pageList: [10, 25, 50, 100, 300, 500, 1000, 2000, 3000, 5000, 10000],
                pagination: true,
                paginationSwitch: true,
                sidePagination: 'server',
                url: "{{ url('') }}/rekap_tunggakan/data_master/Kategori",
                showFooter: true,
                footerStyle: "footerStyle",
                cookie: true,
                queryParams: "queryParams",
                cookieIdTable: "saveId",
                showExport: true,
                clickToSelect: true,
                toolbar: "#toolbar",
                columns: [{
                    checkbox: true,
                    width: 20,
                }, {
                    field: 'id',
                    visible: false
                }, {
                    title: "No",
                    width: 30,
                    halign: "center",
                    align: "center",
                    formatter: "runningFormatter"
                }, {
                    field: "kategori",
                    title: "Kategori",
                    width: 100,
                    halign: "center",
                    align: "center",
                    sortable: true,
                }, {
                    field: "nama",
                    title: "Nama",
                    width: 100,
                    halign: "center",
                    align: "center",
                    sortable: true,
                }, {
                    field: "kelas",
                    title: "Kelas",
                    width: 100,
                    halign: "center",
                    align: "center",
                }, {
                    field: "jurusan",
                    title: "Jurusan",
                    width: 100,
                    halign: "center",
                    align: "center",
                }, {
                    field: "angkatan",
                    title: "Angkatan",
                    width: 100,
                    halign: "center",
                    align: "center",
                }, {
                    field: "besaran",
                    title: "Besaran",
                    width: 150,
                    halign: "center",
                    align: "right",
                    footerFormatter: "totalFormatter",
                }, {
                    field: "potongan",
                    title: "Potongan",
                    width: 150,
                    halign: "center",
                    align: "right",
                    footerFormatter: "totalFormatter",
                }, {
                    field: "terbayar",
                    title: "Terbayar",
                    width: 150,
                    halign: "center",
                    align: "right",
                    footerFormatter: "totalFormatter",
                }, {
                    field: "sisa",
                    title: "Sisa",
                    width: 150,
                    halign: "center",
                    align: "right",
                    footerFormatter: "totalFormatter",
                }, {
                    field: "total",
                    title: "Total",
                    width: 150,
                    halign: "center",
                    align: "right",
                    footerFormatter: "totalFormatter",
                }]
            })
        }).trigger('change')
    });

</script>
@endpush

@push('breadcrumb-left')
<div class="col-md-1" style="item-align:center">
    <a href="{{ url('/rekap')}}" class="btn btn-primary" href="#" title="Kembali"><i class="fa fa-arrow-left"></i></a>
</div>
<div class="col-md-11">
    <div style="margin-left:15px;">
        <h3>Data Laporan Tunggakan</h3>
    </div>
</div>
@endpush


@push('breadcrumb-right')
<div style="float:right">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" style="margin-bottom:0">
            <li class="breadcrumb-item"><a href="{{ url('/')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/rekap')}}">Rekap</a></li>
            <li class="breadcrumb-item active" aria-current="page">Laporan Tunggakan</li>
        </ol>
    </nav>
</div>
@endpush
