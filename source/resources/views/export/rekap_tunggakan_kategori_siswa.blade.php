@extends('export.layout.landscape')

@section('title-html')
{{$title}}
@endsection

@section('title')
{{$title}}
@endsection

@section('content')
<table class="table1">
    <tr>
        <th data-field="no"><div style="text-align:center;">No</div></th>
        <th data-field="kategori"><div style="text-align:center;">Kategori</div></th>
        <th data-field="tanggal"><div style="text-align:center;">Nama</div></th>
        <th data-field="deskripsi"><div style="text-align:center;">Kelas</div></th>
        <th data-field="jurusan"><div style="text-align:center;">Jurusan</div></th>
        <th data-field="angkatan"><div style="text-align:center;">Angkatan</div></th>
        <th data-field="besaran"><div style="text-align:center;">Besaran</div></th>
        <th data-field="potongan"><div style="text-align:center;">Potongan</div></th>
        <th data-field="terbayar"><div style="text-align:center;">Terbayar</div></th>
        <th data-field="sisa"><div style="text-align:center;">Sisa</div></th>
    </tr>
    @php
        $total = [0,0,0,0];
        foreach ($data as $key => $v) {
            if ($v->category->jenis == "Bayar per Bulan") {
                $kelas_x = $v->detail[0]->periode->kelas_x;
                $kelas_xi = $v->detail[0]->periode->kelas_xi;
                $kelas_xii = $v->detail[0]->periode->kelas_xii;
                $besaran = $kelas_x*12 + $kelas_xi*12 + $kelas_xii*12;
                $potongan = 0;
                $terbayar = $v->detail->sum('nominal');
            }
            else
            {
                $besaran = $v->detail[0]->periode->nominal;
                if ( $v->jenis_potongan == "persentase")
                {
                    $potongan = (intval($v->persentase) / 100) * $besaran;
                }
                else
                {
                    $potongan = intval($v->nominal_potongan);
                }
                $terbayar = $v->detail[0]->cicilan->sum('nominal');
            }
            $sisa = $besaran - ($potongan + $terbayar);
            $total[0] += (int) $besaran;
            $total[1] += (int) $potongan;
            $total[2] += (int) $terbayar;
            $total[3] += (int) $sisa;
            @endphp
            <tr>
                <td>{{$no++}}</td>
                <td>{{$v->category->nama}}</td>
                <td>{{$v->student->nama}}</td>
                <td>{{$v->student->kelas}}</td>
                <td>{{$v->student->major->nama}}</td>
                <td>{{$v->student->angkatans->angkatan}} ( {{$v->student->angkatans->tahun}} )</td>
                <td ><div style="text-align:right;">{{number_format($besaran,0,',','.')}}</div></td>
                <td ><div style="text-align:right;">{{number_format($potongan,0,',','.')}}</div></td>
                <td ><div style="text-align:right;">{{number_format($terbayar,0,',','.')}}</div></td>
                <td ><div style="text-align:right;">{{number_format($sisa,0,',','.')}}</div></td>
            </tr>
            @php
        }
    @endphp
    <tr class="footer-section">
        <th colspan="6" style="text-align:center"><span style="font-size:20px;font-weight:bold;">Total </span></th>
        <th style="text-align:right;font-size:12pt;font-weight:bold;">{{number_format($total[0],0,',','.')}}</th>
        <th style="text-align:right;font-size:12pt;font-weight:bold;">{{number_format($total[1],0,',','.')}}</th>
        <th style="text-align:right;font-size:12pt;font-weight:bold;">{{number_format($total[2],0,',','.')}}</th>
        <th style="text-align:right;font-size:12pt;font-weight:bold;">{{number_format($total[3],0,',','.')}}</th>
        <th>&nbsp;</th>
    </tr>
</table>
<small><span style="font-style:italic">Dicetak pada {{now()}}</span></small>

@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        window.print();
    });
</script>
@endpush