@extends('layouts.app')

@section('title')
Setting Biaya SPP | Siswa
@endsection

@section('content')
<!-- Static Table Start -->
<div class="data-table-area mg-b-15">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="sparkline13-list">
                    <div class="sparkline13-hd">
                        <div class="main-sparkline13-hd">
                            <div class="container-sm">
                                <div class="row">
                                    <div class="col-md-6">
                                        <form action="{{route('students.filter')}}" role="form" method="post">
                                        @csrf
                                            <div style="float:left; display:flex; flex-direction:row; max-height:55">
                                            <select class="form-control" name="kelas" required>
                                                <option value="">-- Pilih Kelas -- </option>
                                                <option value="all">Semua</option>
                                                <option @if("X"==$kls) selected @endif value="X">X</option>
                                                <option @if("XI"==$kls) selected @endif value="XI">XI</option>
                                                <option @if("XII"==$kls) selected @endif value="XII">XII</option>
                                                </select>

                                                <select style="margin-left:5px;" class="form-control" name="jurusan" required>
                                                <option value="">-- Pilih Jurusan --</option>
                                                <option value="all">Semua</option>
                                                    @if(isset($majors))
                                                        @foreach($majors as $d)
                                                        <option @if($fil==$d->id) selected @endif value="{{$d->id}}">{{$d->nama}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>

                                                <select style="margin-left:5px;" class="form-control" name="angkatan" required>
                                                <option value="">-- Pilih Angkatan --</option>
                                                <option value="all">Semua</option>
                                                    @if(isset($angkatan))
                                                        @foreach($angkatan as $d)
                                                        <option @if($fil2==$d->id) selected @endif value="{{$d->id}}">{{$d->angkatan}} - {{$d->tahun}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <button type='submit' class="btn btn-info" style="margin-left:5px;">Filter</button>
                                            </div>
                                        </form>
                                    </div>
                                    
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="sparkline13-graph">
                        <div class="datatable-dashv1-list custom-datatable-overright">
                            <div id="toolbar">
                            </div>
                            <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                <thead>
                                    <tr>
                                        <th data-field="id"><div style="text-align:center;">No</div></th>
                                        <th data-field="nis"><div style="text-align:center;">NIS</div></th>
                                        <th data-field="name"><div style="text-align:center;">Nama</div></th>
                                        <th data-field="jenis_kelamin"><div style="text-align:center;">L/P</div></th>
                                        <th data-field="kelas"><div style="text-align:center;">Kelas</div></th>
                                        <th data-field="major"><div style="text-align:center;">Nama Jurusan</div></th>
                                        <th data-field="angkatan"><div style="text-align:center;">Angkatan</div></th>
                                        <th data-field="alamat"><div style="text-align:center;">Biaya SPP</div></th>
                                        <th data-field="action"><div style="text-align:center; padding-left: 100px; padding-right:100px;">Action</div></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(isset($students))
                                @foreach($students as $data)
                                    @php
                                        $temp = explode("-",$data->tgl_masuk);
                                        $date = $temp[2]."/".$temp[1]."/".$temp[0];
                                    @endphp
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$data->nis}}</td>
                                        <td>{{$data->nama_siswa}}</td>
                                        <td><div style="text-align:center;">{{$data->jenis_kelamin}}</div></td>
                                        <td><div style="text-align:center;">{{$data->kelas}}</div></td>
                                        <td><div style="text-align:center;">{{$data->nama}}</div></td>
                                        <td><div style="text-align:center;">{{$data->angkatan}} ({{$data->tahun}})</div></td>
                                        <td><div style="text-align:center;">{{$data->biaya}}</div></td>
                                        <td>
                                        <div style="text-align:center;">
                                          <a href="#" class="btn btn-info" onclick="editConfirm({{$data->id}},'{{$data->nis}}','{{$data->nama_siswa}}','{{$data->nama}}','{{$data->biaya}}')"title="Edit">
                                            <i class="fa fa-eye"> Edit Biaya</i>
                                          </a>
                                       
                                    </div>
                                    </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Static Table End -->
        <!-- modal edit -->
        <div class="modal fade bd-example-modal-lg" id="modalUpdate" tabindex="-1" role="dialog"
            aria-labelledby="modalUpdateLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title" id="modalUpdateLabel">Edit Biaya SPP</h5>
                    </div>
                    <div class="modal-body">
                        <form id='editSiswa' action='' role="form" method="post">
                            @method('PUT')
                            {{csrf_field()}}
                                <input name='id' id='id' placeholder=" Masukan NIS" type='hidden' class='form-control' required>

                            <div class="form-group">
                                <label class="control-label col-md-2">NIS<kode>*</kode></label>
                                <input name='nis' id='nis' placeholder=" Masukan NIS" type='text' class='form-control' required readonly="">
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Nama Siswa<kode>*</kode></label>
                                <input name='nama' id='nama' placeholder=" Masukan Nama Siswa" type='text' class='form-control' required readonly="">
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Jurusan<kode>*</kode></label>
                                <input name='jurusan' id='jurusan' placeholder=" Jurusan" type='text' class='form-control' required readonly="">
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Biaya SPP<kode>*</kode></label>
                                <input name='biaya' id='biaya' placeholder=" Biaya" type='text' class='form-control' required>
                            </div>
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type='submit' class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- hapus -->
        <form id="destroy-form" method="POST">
            @method('DELETE')
            @csrf
        </form>
        @endsection

        @push('styles')
        <!-- x-editor CSS  -->
        <link rel="stylesheet" href="{{ asset('assets/css/editor/select2.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/editor/datetimepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/editor/bootstrap-editable.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/editor/x-editor-style.css') }}">
        <!-- normalize CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/data-table/bootstrap-table.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/data-table/bootstrap-editable.css') }}">

        <!-- forms CSS
        ============================================ -->
        <link rel="stylesheet" href="{{asset('assets/css/form/all-type-forms.css')}}">
        <!-- chosen CSS
        ============================================ -->
        <link rel="stylesheet" href="{{asset('assets/css/chosen/bootstrap-chosen.css')}}">

        <!-- datapicker CSS
        ============================================ -->
        <link rel="stylesheet" href="{{asset('assets/css/datapicker/datepicker3.css')}}">

        <style>
            kode {
                color: red;
            }

        </style>
        @endpush

        @push('scripts')

        <script>
            function peringatanJurusan() {
                swal('Gagal!', 'Sekolah belum mempunyai jurusan. Silahkan diisi terlebih dahulu', 'error')
            }

            function peringatanAngkatan() {
                swal('Gagal!', 'Sekolah belum mempunyai angkatan. Silahkan diisi terlebih dahulu', 'error')
            }

            function show_selected() {
                var selector = document.getElementById('jurusan');
                var value = selector[selector.selectedIndex].value;

                document.getElementById('display').innerHTML = value;
            }
            
            function editConfirm(id,nis,nama,jurusan,biaya) {
                $('#id').attr('value', id);
                $('#nis').attr('value', nis);
                $('#nama').attr('value', nama);
                $('#jurusan').attr('value', jurusan);
                $('#biaya').attr('value', biaya);
                $('#editSiswa').attr('action', "{{ url('setting_biaya/update/') }}/" + id)
                $('#modalUpdate').modal();
            }

            function naikKelas() {
                swal({
                    title: 'Apakah anda yakin?',
                    text: 'Siswa akan naik kelas ?',
                    icon: 'warning',
                    buttons: ["Cancel", "Yes!"],
                }).then(function (value) {
                    if (value) {
                        swal({
                            title: 'Siswa naik kelas ?',
                            text: 'Lanjutkan ?',
                            icon: 'warning',
                            buttons: ["Cancel", "Yes!"],
                        }).then(function (value) {
                            if (value) {
                                var ur = window.location.href+"/naik_kelas";
                                location.replace(ur);
                            } else {
                                swal("Dibatalkan!");
                            }
                        });
                    } else {
                        swal("Dibatalkan!");
                    }
                });
            }
            
            function turunKelas() {
                swal({
                    title: 'Apakah anda yakin?',
                    text: 'Siswa akan turun kelas ?',
                    icon: 'warning',
                    buttons: ["Cancel", "Yes!"],
                }).then(function (value) {
                    if (value) {
                        swal({
                            title: 'Siswa turun kelas ?',
                            text: 'Lanjutkan ?',
                            icon: 'warning',
                            buttons: ["Cancel", "Yes!"],
                        }).then(function (value) {
                            if (value) {
                                var ur = window.location.href+"/turun_kelas";
                                location.replace(ur);
                            } else {
                                swal("Dibatalkan!");
                            }
                        });
                    } else {
                        swal("Dibatalkan!");
                    }
                });
            }


            function destroy(action) {
                swal({
                    title: 'Apakah anda yakin?',
                    text: 'Setelah dihapus, Anda tidak akan dapat mengembalikan data ini!',
                    icon: 'warning',
                    buttons: ["Cancel", "Yes!"],
                }).then(function (value) {
                    if (value) {
                        document.getElementById('destroy-form').setAttribute('action', action);
                        document.getElementById('destroy-form').submit();
                    } else {
                        swal("Data kamu aman!");
                    }
                });
            }

        </script>

        @endpush

        @push('scripts-asset')
        <!-- data table JS
        ============================================ -->
        <script src="{{ asset('assets/js/data-table/bootstrap-table.js') }}"></script>
        <script src="{{ asset('assets/js/data-table/tableExport.js') }}"></script>
        <script src="{{ asset('assets/js/data-table/data-table-active.js') }}"></script>
        <script src="{{ asset('assets/js/data-table/bootstrap-table-editable.js') }}"></script>
        <script src="{{ asset('assets/js/data-table/bootstrap-editable.js') }}"></script>
        <script src="{{ asset('assets/js/data-table/bootstrap-table-resizable.js') }}"></script>
        <script src="{{ asset('assets/js/data-table/colResizable-1.5.source.js') }}"></script>
        <script src="{{ asset('assets/js/data-table/bootstrap-table-export.js') }}"></script>
        <!--  editable JS
        ============================================ -->
        <script src="{{ asset('assets/js/editable/jquery.mockjax.js') }}"></script>
        <script src="{{ asset('assets/js/editable/mock-active.js') }}"></script>
        <script src="{{ asset('assets/js/editable/select2.js') }}"></script>
        <script src="{{ asset('assets/js/editable/moment.min.js') }}"></script>
        <script src="{{ asset('assets/js/editable/bootstrap-datetimepicker.js') }}"></script>
        <script src="{{ asset('assets/js/editable/bootstrap-editable.js') }}"></script>
        <script src="{{ asset('assets/js/editable/xediable-active.js') }}"></script>

        <!-- icheck JS
        ============================================ -->
        <script src="{{ asset('assets/js/icheck/icheck.min.js')}}"></script>
        <script src="{{ asset('assets/js/icheck/icheck-active.js')}}"></script>

        <!-- chosen JS
        ============================================ -->
        <script src="{{ asset('assets/js/chosen/chosen.jquery.js')}}"></script>
        <script src="{{ asset('assets/js/chosen/chosen-active.js')}}"></script>

        <!-- input-mask JS
        ============================================ -->
        <script src="{{ asset('assets/js/input-mask/jasny-bootstrap.min.js')}}"></script>

        <!-- datapicker JS
        ============================================ -->
        <script src="{{ asset('assets/js/datapicker/bootstrap-datepicker.js')}}"></script>
        <script src="{{ asset('assets/js/datapicker/datepicker-active.js')}}"></script>
        @endpush

        @push('breadcrumb-left')
        <h2>Setting Biaya SPP Siswa</h2>
        @endpush


        @push('breadcrumb-right')
        <div style="float:right">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb" style="margin-bottom:0">
                    <li class="breadcrumb-item"><a href="{{ url('/')}}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Siswa</li>
                </ol>
            </nav>
        </div>
        @endpush
